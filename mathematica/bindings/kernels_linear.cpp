// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Mathematica bindings for linear kernel.

#include "mathematica.hpp"
#include "qmmlpack/kernels_linear.hpp"

//  ///////////////////////
//  //  Kernel matrix K  //
//  ///////////////////////

//
//  Computes dot products of vectors from a matrix.
//
//  Input:
//    [0] {Real,2,"Constant"} n x d matrix X of input vectors (rows)
//  Output:
//    [1] {Real,2}  n x n kernel matrix K
//

EXTERN_C DLLEXPORT int qmml_kernel_matrix_linear_k(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 1 ) return LIBRARY_FUNCTION_ERROR;

    MTensor X = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(X) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Xdims = libdata->MTensor_getDimensions(X);
    const mint n = Xdims[0];
    const mint d = Xdims[1];

    MTensor K;
    const mint Kdims[2] = { n, n };
    int err; if( (err = libdata->MTensor_new(MType_Real, 2, Kdims, &K)) ) return err;

    // Calculation
    try
    {
        double const*const xx = libdata->MTensor_getRealData(X);
        double *const kk = libdata->MTensor_getRealData(K);
        qmml::kernel_matrix_linear_k(kk, xx, n, d);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, K);
    return LIBRARY_NO_ERROR;
}


//  ///////////////////////
//  //  Kernel matrix L  //
//  ///////////////////////

//
//  Computes dot products of vectors from two matrices
//
//  Input:
//    [0] {Real,2,"Constant"} n x d matrix X of input vectors (rows)
//    [1] {Real,2,"Constant"} m x d matrix Z of input vectors (rows)
//  Output:
//    [1] {Real,2}  n x m kernel matrix L
//

EXTERN_C DLLEXPORT int qmml_kernel_matrix_linear_l(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 2 ) return LIBRARY_FUNCTION_ERROR;

    MTensor X = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(X) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Xdims = libdata->MTensor_getDimensions(X);
    const mint n = Xdims[0];
    const mint d = Xdims[1];

    MTensor Z = MArgument_getMTensor(args[1]);
    if( libdata->MTensor_getRank(Z) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Zdims = libdata->MTensor_getDimensions(Z);
    const mint m = Zdims[0];
    if( Zdims[1] != d ) return LIBRARY_FUNCTION_ERROR;

    MTensor L;
    const mint Ldims[2] = { n, m };
    int err; if( (err = libdata->MTensor_new(MType_Real, 2, Ldims, &L)) ) return err;

    // Calculation
    try
    {
        double const*const xx = libdata->MTensor_getRealData(X);
        double const*const zz = libdata->MTensor_getRealData(Z);
        double *const ll = libdata->MTensor_getRealData(L);
        qmml::kernel_matrix_linear_l(ll, xx, zz, n, m, d);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, L);
    return LIBRARY_NO_ERROR;
}


//  ///////////////////////
//  //  Kernel vector m  //
//  ///////////////////////

//
//  Computes dot products of vectors with themselves
//
//  Input:
//    [0] {Real,2,"Constant"} n x d matrix X of input vectors (rows)
//  Output:
//    [1] {Real,1} n-vector tr(K).
//

EXTERN_C DLLEXPORT int qmml_kernel_matrix_linear_m(WolframLibraryData libdata, mint argc, MArgument* args, MArgument res)
{
    // Arguments processing
    if( argc != 1 ) return LIBRARY_FUNCTION_ERROR;

    MTensor X = MArgument_getMTensor(args[0]);
    if( libdata->MTensor_getRank(X) != 2 ) return LIBRARY_RANK_ERROR;
    mint const* Xdims = libdata->MTensor_getDimensions(X);
    const mint n = Xdims[0];
    const mint d = Xdims[1];

    MTensor m;
    const mint mdims[1] = { n };
    int err; if( (err = libdata->MTensor_new(MType_Real, 1, mdims, &m)) ) return err;

    // Calculation
    try
    {
        double const*const xx = libdata->MTensor_getRealData(X);
        double *const mm = libdata->MTensor_getRealData(m);
        qmml::kernel_matrix_linear_m(mm, xx, n, d);
    }
    QMML_EXCEPTION_HANDLING(libdata)

    MArgument_setMTensor(res, m);
    return LIBRARY_NO_ERROR;
}
