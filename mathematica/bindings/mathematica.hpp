// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Mathematica bindings

#ifndef QMML_MATHEMATICA_HPP_INCLUDED  // Include guard
#define QMML_MATHEMATICA_HPP_INCLUDED

#include "mathlink.h"        // include before WolframLibrary.h for MathLink (WSTP does not work yet with LibraryLink as of 10.4.1)
#include "WolframLibrary.h"
#include <exception>
#include <new>               // std::bad_alloc
#include "qmmlpack/base.hpp" // qmml::error

// Wolfram Mathematica library error codes:
//   LIBRARY_NO_ERROR         correct result from the function (value of 0)
//   LIBRARY_TYPE_ERROR       unexpected type encountered
//   LIBRARY_RANK_ERROR       unexpected rank encountered
//   LIBRARY_DIMENSION_ERROR  inconsistent dimensions encountered
//   LIBRARY_NUMERICAL_ERROR  error in numerical computation
//   LIBRARY_MEMORY_ERROR     problem allocating memory
//   LIBRARY_FUNCTION_ERROR   generic error from a function

#define QMML_EXCEPTION_HANDLING(LIBDATA) \
catch(std::bad_alloc)                    \
{                                        \
    LIBDATA->Message("qmmlmemory");      \
    return LIBRARY_MEMORY_ERROR;         \
}                                        \
catch(qmml::error & e)                   \
{                                        \
    LIBDATA->Message("qmmlintern");      \
    return LIBRARY_FUNCTION_ERROR;       \
}                                        \
catch(std::exception & e)                \
{                                        \
    LIBDATA->Message("qmmlsystem");      \
    return LIBRARY_FUNCTION_ERROR;       \
}                                        \
catch(...)                               \
{                                        \
    LIBDATA->Message("qmmlunknown");     \
    return LIBRARY_FUNCTION_ERROR;       \
}

#endif // Include guard
