(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2018.    *)
(*  See LICENSE.txt for license. *)

(*  Tests for validation.  *)

Needs["QMMLPack`"];
Needs["ApproximatelyEqual`", "ApproximatelyEqual.m"];
Needs["Developer`"];
On[Assert];

(*  ******************  *)
(*  *  parseOptions  *  *)
(*  ******************  *)

(* simple test cases for different ways to specify values *)
Test[
    {
        (* list input *)
        QMMLPack`Private`parseOptions[
            {1., 2},
            {{"float", 0.}, {"int", 1}, {"string", "hello"}}
        ]
        ,
        (* rules input *)
        QMMLPack`Private`parseOptions[
            {"int" -> 17},
            {{"float", 0.}, {"int", 1}, {"string", "hello"}}
        ]
        ,
        (* association input *)
        QMMLPack`Private`parseOptions[
            <|"float" -> -1., "another" -> "test"|>,
            {{"float", 0.}, {"int", 1}, {"string", "hello"}}
        ]
    }
    ,
    {
        {1., 2, "hello"},
        {0., 17, "hello"},
        {-1., 1, "hello"}
    }
    ,
    TestID->"Init-20180511-parseOptions-Ae8"
]

(* empty values *)
Test[
    {
        (* list/rules input *)
        QMMLPack`Private`parseOptions[
            {},
            {{"float", 0.}, {"int", 1}, {"string", "hello"}}
        ]
        ,
        (* rules input *)
        QMMLPack`Private`parseOptions[
            {},
            {{"float", 0.}, {"int", 1}, {"string", "hello"}}
        ]
        ,
        (* association input *)
        QMMLPack`Private`parseOptions[
            <||>,
            {{"float", 0.}, {"int", 1}, {"string", "hello"}}
        ]
    }
    ,
    {
        {0., 1, "hello"},
        {0., 1, "hello"},
        {0., 1, "hello"}
    }
    ,
    TestID->"Init-20180511-parseOptions-rr1"
]
