(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(*  Tests for kernel functions.  *)

Needs["QMMLPack`"];
Needs["ApproximatelyEqual`", "ApproximatelyEqual.m"];
Needs["Developer`"];
On[Assert];


(*  ************************  *)
(*  *  CenterKernelMatrix  *  *)
(*  ************************  *)

(* Special case of no training samples. If no training samples are given, the mean is not defined in feature space (division by zero) *)
Test[
    {
        CenterKernelMatrix[{{}}],
        CenterKernelMatrix[{{}},{{}}],
        CenterKernelMatrix[{{}},{{1,0},{0,1}}],
        CenterKernelMatrix[{{}},{{}},{{}}],
        CenterKernelMatrix[{{}},{{}},{}],
        CenterKernelMatrix[{{}},{{}},{{2,1},{1,2}}],
        CenterKernelMatrix[{{}},{{}},{2,2}],
        CenterKernelMatrix[{{}},{{2,1},{1,2}},{{}}],
        CenterKernelMatrix[{{}},{{2,1},{1,2}},{}]
    }
    ,
    ConstantArray[Indeterminate, 9]
    ,
    TestID->"Kernels-20160722-J7N8R3"
]

(* Simple example: 4 corners of a square in the plane *)
Test[
    Module[{K,L,M},
        K = KernelLinear[{{0,0},{2,0},{2,2},{0,2}}, {}];
        L = KernelLinear[{{0,0},{2,0},{2,2},{0,2}}, {{1,0},{2,1},{1,2},{0,1},{1,1}}, {}];
        M = KernelLinear[{{1,0},{2,1},{1,2},{0,1},{1,1}}, {}];
        {CenterKernelMatrix[K], CenterKernelMatrix[K,L], CenterKernelMatrix[K,L,M], CenterKernelMatrix[K,L,Diagonal[M]]}
    ]
    ,
    N[{
        KernelLinear[{{-1,-1},{1,-1},{1,1},{-1,1}}, {}],
        {{1,-1,-1,1,0},{1,1,-1,-1,0},{-1,1,1,-1,0},{-1,-1,1,1,0}},
        {{1,0,-1,0,0},{0,1,0,-1,0},{-1,0,1,0,0},{0,-1,0,1,0},{0,0,0,0,0}},
        Diagonal[{{1,0,-1,0,0},{0,1,0,-1,0},{-1,0,1,0,0},{0,-1,0,1,0},{0,0,0,0,0}}]
    }]
    ,
    TestID->"Kernels-20160722-I8P9P2"
]

(* Already centered matrix *)
Test[
    Module[{X,XX,K,L,M},
        X  = { { 1, 0 }, { 0, 1 }, { -1, 0 }, {  0, -1 } }; (* already centered *)
        XX = { { 0, 0 }, { 1, 1 }, { -1, 1 }, { -1, -1 }, { 1, -1 } };
        K = KernelLinear[X, {}];
        L = KernelLinear[X, XX, {}];
        M = KernelLinear[XX, {}];
	    {
            CenterKernelMatrix[K],
            CenterKernelMatrix[K, L],
            CenterKernelMatrix[K, L, M],
            CenterKernelMatrix[K, L, Diagonal[M]]
        }
    ]
	,
    {
	   KernelLinear[{{1,0},{0,1},{-1,0},{0,-1}}, {}], (* K *)
       KernelLinear[{{1,0},{0,1},{-1,0},{0,-1}}, {{0,0},{1,1},{-1,1},{-1,-1},{1,-1}}, {}], (* L *)
       KernelLinear[{{0,0},{1,1},{-1,1},{-1,-1},{1,-1}}, {}], (* M *)
       KernelLinear[{{0,0},{1,1},{-1,1},{-1,-1},{1,-1}}, {}, Diagonal->True] (* Diagonal[M] *)
    }
	,
	TestID->"Kernels-20160723-T3N1D4"
]

(* Explicit centering in the linear case *)
Test[
    Module[{X, Xc},
        X = RandomVariate[UniformDistribution[{-1, 1}], {5, 7}];
        Xc = Standardize[X, Mean, 1 &]; (* Shifts by Mean, no rescaling *)
        Chop[ CenterKernelMatrix[KernelLinear[X, {}]] - KernelLinear[Xc, {}] ]
    ]
	,
	ConstantArray[0, {5, 5}]
	,
	TestID->"Kernels-20160723-B9K6P4"
]

(* Randomized test versus Mathematica version *)
Test[
	Module[{X,Z,K,L,M,centerKernelMatrixK,centerKernelMatrixL,centerKernelMatrixM,centerKernelMatrixMdiag},

	    X = RandomReal[{-1,1}, {15, 5}];
	    Z = RandomReal[{-1,1}, {25, 5}];
	    K = KernelGaussian[X, {1}];
	    L = KernelGaussian[X, Z, {1}];
	    M = KernelGaussian[Z, {1}];

        centerKernelMatrixK[K_] := Module[{n = Length[K], onesn},
            onesn = IdentityMatrix[n] - ConstantArray[1, {n, n}] / n;  (* onesn = identity (n,n) - (1/n)*ones (n,n) *)
            onesn.K.onesn  (* Symmetrize matrix as (Kc + Transpose[Kc]) / 2 to compensate for numerical noise? *)
        ];

        centerKernelMatrixL[K_, L_] := Module[{n = Length[K], m = Dimensions[L][[2]], onesnn, onesnm},
            onesnn = ConstantArray[1, {n,n}] / n;
            onesnm = ConstantArray[1, {n,m}] / n;
            L - K . onesnm - onesnn . L + onesnn . K . onesnm
        ];

        centerKernelMatrixM[K_, L_, M_] := Module[{n = Length[K], m = Dimensions[L][[2]], onesnm, onesmn},
            onesnm = ConstantArray[1, {n,m}] / n;
            onesmn = ConstantArray[1, {m,n}] / n;
            M - Transpose[L] . onesnm - onesmn . L + onesmn . K . onesnm
        ];

        centerKernelMatrixMdiag[K_, L_, mv_] := Module[{m = Dimensions[L][[2]]},
            mv - 2*Mean[L] + ConstantArray[Mean[Flatten[K]], m]
        ];

        {
            Total[Chop[Abs[Flatten[centerKernelMatrixK[K] - CenterKernelMatrix[K]]]]],
            Total[Chop[Abs[Flatten[centerKernelMatrixL[K,L] - CenterKernelMatrix[K,L]]]]],
            Total[Chop[Abs[Flatten[centerKernelMatrixM[K,L,M] - CenterKernelMatrix[K,L,M]]]]],
            Total[Chop[Abs[Flatten[centerKernelMatrixMdiag[K,L,Diagonal[M]] - CenterKernelMatrix[K,L,Diagonal[M]]]]]]
        }
	]
	,
	ConstantArray[0, 4]
	,
	TestID->"Kernels-20110708-P8C2H6"
]

(* Symmetry *)
Test[
    {
        SymmetricMatrixQ[CenterKernelMatrix[KernelLinear[RandomReal[{-1,1}, {100, 50}], {}]]],  (* From linear kernel *)
        SymmetricMatrixQ[CenterKernelMatrix[KernelGaussian[RandomReal[{-1,1}, {100, 50}], {5}]]]  (* From Gaussian kernel *)
    }
    ,
    {True, True}
    ,
    TestID->"Kernels-20160923-eR893W"
]


(*  *******************  *)
(*  *  Linear kernel  *  *)
(*  *******************  *)

Test[
    {
        (* K, one sample *)
        KernelLinear[{{1,2,3}},{}],
        (* K, two samples *)
        KernelLinear[{{1,-1},{2,3}}, {}],
        (* L, one sample each *)
        KernelLinear[{{1,-1}}, {{2,3}}, {}],
        KernelLinear[{{1,2,3}}, {{2,0,-1}}, {}],
        (* L, two versus one *)
        KernelLinear[{{1,2}, {3,4}}, {{-1,-2}}, {}],
        (* L, one versus two *)
        KernelLinear[{{3,1}}, {{5,6}, {0,1}}, {}],
        (* L, two versus two *)
        KernelLinear[{{1,2}, {3,5}}, {{-2,-1}, {-4,-3}}, {}],
        (* L, three versus two *)
        KernelLinear[{{1,-1}, {2,3}, {-1,2}}, {{-1,-1}, {4,5}}, {}],
        KernelLinear[{{-2,-1},{-4,-3},{-6,-5}}, {{1,2},{3,4}}, {}],
        (* L, two versus three *)
        KernelLinear[{{1,2},{3,4}}, {{-2,-1},{-4,-3},{-6,-5}}, {}],
        (* M, one sample *)
        KernelLinear[{{1,2,3}},{}, Diagonal->True],
        (* M, two samples *)
        KernelLinear[{{1,-1},{2,3}},{},Diagonal->True]
    }
    ,
    {
        {{14.}},
        {{2.,-1.},{-1.,13.}},
        {{-1.}},
        {{-1.}},
        {{-5.}, {-11.}},
        {{21., 1.}},
        {{-4., -10.}, {-11., -27.}},
        {{0.,-1.},{-5.,23.},{-1.,6.}},
        {{-4.,-10.},{-10.,-24.},{-16.,-38.}},
        {{-4.,-10.,-16.},{-10.,-24.,-38.}},
        {14.},
        {2.,13.}
    }
    ,
    TestID->"Kernels-20110208-Z0U7X6"
]

Test[
    {
        PackedArrayQ[KernelLinear[{{1,2,3},{4,5,6}}, {}]],
        PackedArrayQ[KernelLinear[{{1,2,3},{4,5,6}}, {{7,8,9},{-1,-2,-3}}, {}]],
        PackedArrayQ[KernelLinear[{{1,2},{3,4},{5,6}}, {}, Diagonal->True]]
    }
    ,
    ConstantArray[True, 3]
    ,
    TestID->"Kernels-20130831-T5H0I4"
]

(* Symmetry *)
Test[
    {
        SymmetricMatrixQ[KernelLinear[RandomReal[{-1,1}, {   2,   1}], {}]],
        SymmetricMatrixQ[KernelLinear[RandomReal[{-1,1}, {  20,  10}], {}]],
        SymmetricMatrixQ[KernelLinear[RandomReal[{-1,1}, { 100, 500}], {}]],
        SymmetricMatrixQ[KernelLinear[RandomReal[{-1,1}, {1000, 100}], {}]]
    }
    ,
    ConstantArray[True, 4]
    ,
    TestID->"Kernels-20160923-pOed90"
]


(*  *********************  *)
(*  *  Gaussian kernel  *  *)
(*  *********************  *)

(* D *)
Test[
    And@@
    {
        KernelGaussian[{ { 0 } }, {1.}, Distance->True] == { { 1. } },  (* one sample *)
        KernelGaussian[{ { 0, 17 }, { 17, 0 } }, {1.}, Distance->True] == N[{ { 1, Exp[-17/2] }, { Exp[-17/2], 1 } }],  (* two samples *)
        KernelGaussian[{ { 0, 8 }, { 8, 0 } }, {2.}, Distance->True] == N[{ { 1, 1/Exp[1] }, { 1/Exp[1], 1 } }]  (* two samples *)
    }
    ,
    True
    ,
    TestID->"Kernels-20160713-KIe4"
]

(* E *)
Test[
    And@@
    {
        KernelGaussian[{ { 1 } }, {Sqrt[0.5]}, Distance->True] == N[{ { Exp[-1] } }],  (* one vs one *)
        KernelGaussian[{ { 4, 45 }, { 25, 8 }, { 9, 34 } }, {1.}, Distance->True] == N[Exp[{ { -2, -45/2 }, { -25/2, -4 }, { -9/2, -17 } }]],  (* three vs two *)
        KernelGaussian[{ { 10, 2, 4 }, { 5, 5, 1 } }, {Sqrt[0.5]}, Distance->True] == N[Exp[{ { -10, -2, -4 }, { -5, -5, -1 } }]] (* two vs three *)
    }
    ,
    True
    ,
    TestID->"Kernels-20160713-3EerAA"
]

(* K *)
Test[
    And@@
    {
        KernelGaussian[{ { 1 } }, {1.}] == N[{ { 1 } }], (* one sample *)
        KernelGaussian[{ { 1, -1 }, { 2, 3 } }, {1.}] == N[{ { 1, Exp[-17/2] }, { Exp[-17/2], 1 } }], (* two samples *)
        KernelGaussian[{ { 1, 2 }, { 3, 4 } }, {2.}] == N[{ { 1, 1/Exp[1] }, { 1/Exp[1], 1 } }]  (* two samples *)
    }
    ,
    True
    ,
    TestID->"Kernels-20160713-Pe4j4L"
]

(* L *)
Test[
    And@@
    {
        KernelGaussian[{ { 1 } }, { { 2 } }, {Sqrt[0.5]}] == N[{ { Exp[-1] } }],  (* one vs one *)
        KernelGaussian[{ { 1, -1 } }, { { 2, 3 } }, {1.}] == N[{ { Exp[-17/2] } }], (* one vs one *)
        KernelGaussian[{ { 1, -1 }, { 2, 3 }, { -1, 2 } }, { { -1, -1 }, { 4, 5 } }, {1.}] == N[Exp[{ { -2, -45/2 }, { -25/2, -4 }, { -9/2, -17 } }]], (* three vs two *)
        KernelGaussian[{ { -2, 1 }, { 0, 1 }, { -1, 2 } }, { { 1, 2 }, { -1, 3 } }, {Sqrt[1/2]}] == N[Exp[{ { -10, -5 }, { -2, -5 }, { -4, -1 } }]],  (* three vs two *)
        KernelGaussian[{ { 1, 2 }, { -1, 3 } }, { { -2, 1 }, { 0, 1 }, { -1, 2 } }, {Sqrt[1/2]}] == N[Exp[{ { -10, -2, -4 }, { -5, -5, -1 } }]]  (* two vs three *)
    }
    ,
    True
    ,
    TestID->"Kernels-20160713-4IoOp2"
]

(* M *)
Test[
    And@@
    {
        KernelGaussian[{ { 1 } }, {10.}, Diagonal->True] == N[{ 1 }],  (* one sample, length 1 *)
        KernelGaussian[{ { 1, 2, 3 } }, {1.}, Diagonal->True] == N[{ 1 }],  (* one sample, length 3 *)
        KernelGaussian[{ { 1, 2 }, { 3, 4 }, { 5, 6 } }, {1.}, Diagonal->True] == N[{ 1, 1, 1}]  (* three samples *)
    }
    ,
    True
    ,
    TestID->"Kernels-20160713-Fkdi8e"
]

(* Mathematica bindings *)
Test[
    And@@
    {
        PackedArrayQ[KernelGaussian[{{1,2,3},{4,5,6}}, {1}]], (* K *)
        PackedArrayQ[KernelGaussian[{{1,2,3},{4,5,6}}, {{7,8,9},{-1,-2,-3}}, {1}]], (* Z *)
        PackedArrayQ[KernelGaussian[{{1,2},{3,4},{5,6}}, {1}, Diagonal->True]], (* K, diagonal *)
        PackedArrayQ[KernelGaussian[{{1,2,3},{4,5,6},{7,8,9}}, {1}, Distance->True]], (* K distance *)
        PackedArrayQ[KernelGaussian[{{1,2,3},{4,5,6},{7,8,9}}, {1}, Distance->True, Diagonal->True]] (* K distance, diagonal *)
    }
    ,
    True
    ,
    TestID->"Kernels-20120415-D3U2U9"
]

(* Symmetry, from distance matrix *)
Test[
    {
        SymmetricMatrixQ[KernelGaussian[DistanceSquaredEuclidean[RandomReal[{-1,1}, {   2,   1}]], { 1}, Distance->True]],
        SymmetricMatrixQ[KernelGaussian[DistanceSquaredEuclidean[RandomReal[{-1,1}, {  20,  10}]], { 2}, Distance->True]],
        SymmetricMatrixQ[KernelGaussian[DistanceSquaredEuclidean[RandomReal[{-1,1}, { 100, 500}]], {13}, Distance->True]],
        SymmetricMatrixQ[KernelGaussian[DistanceSquaredEuclidean[RandomReal[{-1,1}, {1000, 100}]], { 6}, Distance->True]]
    }
    ,
    ConstantArray[True, 4]
    ,
    TestID->"Kernels-20160923-Jed15A"
]

(* Symmetry, from input *)
Test[
    {
        SymmetricMatrixQ[KernelGaussian[RandomReal[{-1,1}, {   2,   1}], { 1}]],
        SymmetricMatrixQ[KernelGaussian[RandomReal[{-1,1}, {  20,  10}], { 2}]],
        SymmetricMatrixQ[KernelGaussian[RandomReal[{-1,1}, { 100, 500}], {13}]],
        SymmetricMatrixQ[KernelGaussian[RandomReal[{-1,1}, {1000, 100}], { 6}]]
    }
    ,
    ConstantArray[True, 4]
    ,
    TestID->"Kernels-20160923-Jed45L"
]


(*  **********************  *)
(*  *  Laplacian kernel  *  *)
(*  **********************  *)

(* D *)
Test[
    {
        KernelLaplacian[{ { 0 } }, {1.}, Distance->True] == { { 1. } },  (* one sample *)
        KernelLaplacian[{ { 0, 17 }, { 17, 0 } }, {2.}, Distance->True] == N[{ { 1, Exp[-17/2] }, { Exp[-17/2], 1 } }],  (* two samples 1 *)
        KernelLaplacian[{ { 0, 8 }, { 8, 0 } }, {4.}, Distance->True] == N[{ { 1, Exp[-2] }, { Exp[-2], 1 } }],  (* two samples 2 *)
        KernelLaplacian[{ { 0, 17 }, { 17, 0 } }, {1.}, Distance->True, Diagonal->True] == N[{ 1, 1 }] (* diagonal *)
    }
    ,
    ConstantArray[True, 4]
    ,
    TestID->"Kernels-20160804-lOp34d"
]

(* E *)
Test[
    {
        KernelLaplacian[{ { 1 } }, {1.}, Distance->True] == N[{ { Exp[-1] } }],  (* one vs one *)
        KernelLaplacian[{ { 2, 9 }, { 7, 4 }, { 3, 8 } }, {0.5}, Distance->True] == N[Exp[{ { -4, -18 }, { -14, -8 }, { -6, -16 } }]],  (* three vs two *)
        KernelLaplacian[{ { 4, 2, 2 }, { 3, 3, 1 } }, {0.5}, Distance->True] == N[Exp[{ { -8, -4, -4 }, { -6, -6, -2 } }]] (* two vs three *)
    }
    ,
    ConstantArray[True, 3]
    ,
    TestID->"Kernels-20160804-3FFrAA"
]

(* K *)
Test[
    {
        KernelLaplacian[{ { 1 } }, {1.}] == N[{ { 1 } }], (* one sample *)
        KernelLaplacian[{ { 1, -1 }, { 2, 3 } }, {1.}] == N[{ { 1, Exp[-5] }, { Exp[-5], 1 } }], (* two samples *)
        KernelLaplacian[{ { 1, 2 }, { 3, 4 } }, {2.}] == N[{ { 1, Exp[-2] }, { Exp[-2], 1 } }]  (* two samples *)
    }
    ,
    ConstantArray[True, 3]
    ,
    TestID->"Kernels-20160804-PA4j4L"
]

(* L *)
Test[
    {
        KernelLaplacian[{ { 1 } }, { { 2 } }, {1.}] == N[{ { Exp[-1] } }],  (* one vs one *)
        KernelLaplacian[{ { 1, -1 } }, { { 2, 3 } }, {1.}] == N[{ { Exp[-5] } }], (* one vs one *)
        KernelLaplacian[{ { 1, -1 }, { 2, 3 }, { -1, 2 } }, { { -1, -1 }, { 4, 5 } }, {1.}] == N[Exp[{ { -2, -9 }, { -7, -4 }, { -3, -8 } }]], (* three vs two *)
        KernelLaplacian[{ { -2, 1 }, { 0, 1 }, { -1, 2 } }, { { 1, 2 }, { -1, 3 } }, {1/2}] == N[Exp[{ { -8, -6 }, { -4, -6 }, { -4, -2 } }]],  (* three vs two *)
        KernelLaplacian[{ { 1, 2 }, { -1, 3 } }, { { -2, 1 }, { 0, 1 }, { -1, 2 } }, {1/2}] == N[Exp[{ { -8, -4, -4 }, { -6, -6, -2 } }]]  (* two vs three *)
    }
    ,
    ConstantArray[True, 5]
    ,
    TestID->"Kernels-20160804-oeq89A"
]

(* M *)
Test[
    {
        KernelLaplacian[{ { 1 } }, {10.}, Diagonal->True] == N[{ 1 }],  (* one sample, length 1 *)
        KernelLaplacian[{ { 1, 2, 3 } }, {1.}, Diagonal->True] == N[{ 1 }],  (* one sample, length 3 *)
        KernelLaplacian[{ { 1, 2 }, { 3, 4 }, { 5, 6 } }, {1.}, Diagonal->True] == N[{ 1, 1, 1}]  (* three samples *)
    }
    ,
    ConstantArray[True, 3]
    ,
    TestID->"Kernels-20160804-Fkei8e"
]

(* Mathematica bindings *)
Test[
    {
        PackedArrayQ[KernelLaplacian[{{1,2,3},{4,5,6}}, {1}]], (* K *)
        PackedArrayQ[KernelLaplacian[{{1,2,3},{4,5,6}}, {{7,8,9},{-1,-2,-3}}, {1}]], (* Z *)
        PackedArrayQ[KernelLaplacian[{{1,2},{3,4},{5,6}}, {1}, Diagonal->True]], (* K, diagonal *)
        PackedArrayQ[KernelLaplacian[{{1,2,3},{4,5,6},{7,8,9}}, {1}, Distance->True]], (* K distance *)
        PackedArrayQ[KernelLaplacian[{{1,2,3},{4,5,6},{7,8,9}}, {1}, Distance->True, Diagonal->True]] (* K distance, diagonal *)
    }
    ,
    ConstantArray[True, 5]
    ,
    TestID->"Kernels-20160804-D3U2U9"
]

(* Symmetry, from distance matrix *)
Test[
    {
        SymmetricMatrixQ[KernelLaplacian[DistanceOneNorm[RandomReal[{-1,1}, {   2,   1}]], {  1}, Distance->True]],
        SymmetricMatrixQ[KernelLaplacian[DistanceOneNorm[RandomReal[{-1,1}, {  20,  10}]], {  6}, Distance->True]],
        SymmetricMatrixQ[KernelLaplacian[DistanceOneNorm[RandomReal[{-1,1}, { 100, 500}]], {331}, Distance->True]],
        SymmetricMatrixQ[KernelLaplacian[DistanceOneNorm[RandomReal[{-1,1}, {1000, 100}]], { 67}, Distance->True]]
    }
    ,
    ConstantArray[True, 4]
    ,
    TestID->"Kernels-20160923-ABCde4"
]

(* Symmetry, from input *)
Test[
    {
        SymmetricMatrixQ[KernelLaplacian[RandomReal[{-1,1}, {   2,   1}], {  1}]],
        SymmetricMatrixQ[KernelLaplacian[RandomReal[{-1,1}, {  20,  10}], {  6}]],
        SymmetricMatrixQ[KernelLaplacian[RandomReal[{-1,1}, { 100, 500}], {331}]],
        SymmetricMatrixQ[KernelLaplacian[RandomReal[{-1,1}, {1000, 100}], { 67}]]
    }
    ,
    ConstantArray[True, 4]
    ,
    TestID->"Kernels-20160923-j49Iek"
]
