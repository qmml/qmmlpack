(* ::Package:: *)

(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(*  Mathematica package initialization *)

(*  *************  *)
(*  *  Library  *  *)
(*  *************  *)

(* This part is in the init.m file because it is needed by most other .m files of the library *)

BeginPackage["QMMLPack`", {"LibraryLink`"}]

Begin["`Private`"]

QMMLPackLoader::error = "`1`.";

Block[{libpath, libfn},
    (* Preload dynamic library *)
    libpath = FileNameJoin[Join[Drop[FileNameSplit[FindFile["QMMLPack`"]], -2], {"LibraryResources", $SystemID}]];
    libfn = Switch[$SystemID,
        "MacOSX-x86-64", "libqmmlpackmm.*.dylib",
        "Linux-x86-64" , "libqmmlpackmm.so.*.*",
        "Windows"      , (Message[QMMLPackLoader::error, "Microsoft Windows operating system currently not supported"]; Abort[];),
        _              , (Message[QMMLPackLoader::error, "Unknown operating system returned by $SystemID"]; Abort[];)
    ];
    libfn = FileNames[libfn, libpath];
    Switch[Length[libfn],
        0, (Message[QMMLPackLoader::error, "Mathematica bindings library not found"]; Abort[];),
        1, libfn = First[libfn];,
        _, (Message[QMMLPackLoader::error, "Multiple Mathematica bindings libraries found"]; Abort[];)
    ];
    libQMMLPack = FindLibrary[libfn];  (* QMMLPack`Private`libQMMLPack *)
    If[libQMMLPack === $Failed, (Message[QMMLPackLoader::error, StringForm["Failed to find Mathematica bindings library ``", libfn]];Abort[];)];
    If[LibraryLoad[libfn] === $Failed, (Message[QMMLPackLoader::error, StringForm["Failed to load Mathematica bindings library ``", libfn]]; Abort[];)];
];

LibraryFunctionCheckedLoad[lib_, fun_, argtype_, rettype_] := Module[{f},
    f = LibraryFunctionLoad[lib, fun, argtype, rettype];
    If[f === $Failed, Message[QMMLPackLoader::error, "Failed to load function '" <> ToString[fun] <> "'."]];
    f
];

LibraryFunction::qmmlmemory  = "QMMLPack: error allocating memory.";
LibraryFunction::qmmlintern  = "QMMLPack: internal error (qmml::error exception).";
LibraryFunction::qmmlsystem  = "QMMLPack: internal error (C++/STL exception).";
LibraryFunction::qmmlunknown = "QMMLPack: internal error (unknown exception).";

AppendTo[$MessageGroups, "QMMLPackLibrary" :> {LibraryFunction::qmmlmemory, LibraryFunction::qmmlintern, LibraryFunction::qmmlsystem, LibraryFunction::qmmlunknown,
    LibraryFunction::mbtrinvnarg, LibraryFunction::mbtrinvk, LibraryFunction::mbtrinvsizes, LibraryFunction::mbtrinvinds, LibraryFunction::mbtrinvz,
    LibraryFunction::mbtrinvr, LibraryFunction::mbtrinvb, LibraryFunction::mbtrinvdiscr, LibraryFunction::mbtrinvgeomf, LibraryFunction::mbtrinvweightf,
    LibraryFunction::mbtrinvdistrf, LibraryFunction::mbtrinvcorrf, LibraryFunction::mbtrinveindexf, LibraryFunction::mbtrinvaindexf, LibraryFunction::mbtrinvacc,
    LibraryFunction::mbtrint, LibraryFunction::mbtrresfail
}];

End[]

EndPackage[]


(*  ***********************************  *)
(*  *  Library-wide utility functions  *  *)
(*  ***********************************  *)

BeginPackage["QMMLPack`"]

(* parseOptions symbol is not exported via usage field as it is intended for package-internal use only *)

QMMLPack::error = "Internal error: `1`";

Begin["`Private`"]

parseOptions[values_, defaults_] := (
    If[Head[values] === List,
        If[Length[values] == 0, Return[defaults[[All, 2]]]]; (* can not and need not determine if lists of values or rules *)
        If[Head[First[values]] === Rule,
            (* list of rules *)
            Return[parseOptions[Association[values], defaults]];
        ,
            (* list of values *)
            Return[MapIndexed[If[First[#2] <= Length[values], values[[First[#2]]], #1[[2]]] &, defaults]];
        ];
    ];
    If[Head[values] === Association,
        Return[Map[If[KeyExistsQ[values, #[[1]]], values[#[[1]]], #[[2]]] &, defaults]];
    ];
    Message[QMMLPack::error, "Invalid argument to parseOptions."];
    $Failed
);

End[]

EndPackage[]


(*  ******************  *)
(*  *  Source files  *  *)
(*  ******************  *)

Get["QMMLPack`Utility`"]
Get["QMMLPack`InputOutput`"]
Get["QMMLPack`Distances`"]
Get["QMMLPack`Kernels`"]
Get["QMMLPack`Numerics`"]
Get["QMMLPack`Validation`"]
Get["QMMLPack`Regression`"]
Get["QMMLPack`Representations`"]
