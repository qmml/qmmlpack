(* ::Package:: *)

(*  qmmlpack                     *)
(*  Matthias Rupp, 2016.         *)
(*  See LICENSE.txt for license. *)

(* Numerics utilities *)

BeginPackage["QMMLPack`", {"LibraryLink`"}]


(*  ***************  *)
(*  *  linearize  *  *)
(*  ***************  *)

If[Not[ValueQ[linearize::usage]], linearize::usage =
"linearize[a] returns linearized array and position indices.

See Algorithm on \"Linearizing and delinearizing arrays\".

Parameters:
  a - ragged array (list of lists)

Options:
  Subset [All] - subset of indices to consider

Returns:
  Tuple {b,s} where b is the linearized array and s are position indices."];

Begin["`Private`"]

Options[linearize] = {Subset -> None};

linearize[a_, opts: OptionsPattern[]] := Module[{osubset, s},
    (* option handling *)
    osubset = OptionValue[Subset];
    osubset = If[osubset === None, All, osubset];

    (* index array and linearized array *)
    s = Prepend[Accumulate[Map[Length, a[[osubset]]]] + 1, 1];
    {Flatten[a[[osubset]]], s}
]

End[]

(*  *****************  *)
(*  *  delinearize  *  *)
(*  *****************  *)

If[Not[ValueQ[delinearize::usage]], delinearize::usage =
"delinearize[a, s] returns a ragged array

See Algorithm on \"Linearizing and delinearizing arrays\".

Parameters:
  a - linearized array (flat list)
  s - array of positions (1-based)

Options:
  Subset [All] - subset of indices to consider

Returns:
  Ragged array (list of lists) b"];

Begin["Private`"]

Options[delinearize] = {Subset -> None};

delinearize[a_, s_, opts:OptionsPattern[]] := Module[{osubset},
    (* Options handling *)
    osubset = OptionValue[Subset];
    osubset = If[osubset === None, All, osubset];

    (* ragged array *)
    MapThread[a[[#1;;#2-1]] &, {Most[s][[osubset]], Rest[s][[osubset]]}]
]

End[]

(*  *************************  *)
(*  *  LowerTriangularPart  *  *)
(*  *************************  *)

If[Not[ValueQ[LowerTriangularPart::usage]], LowerTriangularPart::usage =
"LowerTriangularPart[m] returns the lower triangular part of matrix m.
LowerTriangularPart[m,k] uses only elements on and below the k-th main diagonal of m.

Example:
  LowerTriangularPart[{{a,b},{c,d}}] returns {a,c,d}.
  LowerTriangularPart[{{a,b},{c,d}}, -1] returns {c}.
  LowerTriangularPart[m] includes the main diagonal, LowerTriangularPart[m,-1] does not."];

Begin["`Private`"]

lowerTriangularPart = LibraryFunctionLoad[libQMMLPack, "qmml_lower_triangular_part", {{Real, 2, "Constant"}, {Integer, 0}}, {Real, 1}];

LowerTriangularPart[m_?(MatrixQ[#,NumericQ]&)] := lowerTriangularPart[m, 0];
LowerTriangularPart[m_?(MatrixQ[#,NumericQ]&), k_Integer] := lowerTriangularPart[m, k];

(* If the necessity for results of the form {{a},{b,c},{d,e,f,},...} should come up,
   add an option Flatten with default value True. Using UpValues (Flatten[LowerTr...[...]])
   seems not doable because the more specific LowerTriang...[...] would be evaluated first. *)

End[]


(*  ****************  *)
(*  *  Symmetrize  *  *)
(*  ****************  *)

(* Mathematica already has a function Symmetrize[m], with identical result for Normal[Symmetrize[m]]. *)

(* see http://mathematica.stackexchange.com/questions/57257/how-to-unprotect-a-variable-in-a-package *)

Off[General::shdw]
QMMLPack`Symmetrize
On[General::shdw]

Begin["`Private`"]

symmetrize = LibraryFunctionLoad[libQMMLPack, "qmml_symmetrize", {{Real, 2}}, {Real, 2}];

Symmetrize[m_?(MatrixQ[#, NumericQ] && Dimensions[#][[1]] == Dimensions[#][[2]] &)] := symmetrize[m];

End[]


(*  *************************  *)
(*  *  ForwardSubstitution  *  *)
(*  *************************  *)

If[Not[ValueQ[ForwardSubstitution::usage]], ForwardSubstitution::usage =
"ForwardSubstitution[L,b] solves L.x = b for lower triangular matrix L and vector b.
ForwardSubstitution[L,B] solves L.X = B for matrix B.
Example:
  Let L . U = M (Cholesky decomposition) for a symmetric positive definite matrix M. \
  Then BackwardSubstitution[U, ForwardSubstitution[L, IdentityMatrix[Dimensions[M]]]] is the inverse of M."];

Begin["`Private`"]

forwardSubstitutionV = LibraryFunctionLoad[libQMMLPack, "qmml_forward_substitution_v", {{Real, 2, "Constant"}, {Real, 1, "Constant"}}, {Real, 1}];
forwardSubstitutionM = LibraryFunctionLoad[libQMMLPack, "qmml_forward_substitution_m", {{Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}];

ForwardSubstitution[{{}}, {}] = {};
ForwardSubstitution[{{}}, {{}}] = {{}};

ForwardSubstitution[L_?(MatrixQ[#, NumericQ] &), b_?(VectorQ[#, NumericQ] &)] := forwardSubstitutionV[L, b];
ForwardSubstitution[L_?(MatrixQ[#, NumericQ] &), B_?(MatrixQ[#, NumericQ] &)] := forwardSubstitutionM[L, B];

End[]


(*  **************************  *)
(*  *  BackwardSubstitution  *  *)
(*  **************************  *)

If[Not[ValueQ[BackwardSubstitution::usage]], BackwardSubstitution::usage =
"BackwardSubstitution[U,b] solves U.x = b for upper triangular matrix U and vector b.
BackwardSubstitution[U,B] solves U.X = B for matrix B.
Example:
  Let L . U = M (Cholesky decomposition) for a symmetric positive definite matrix M. \
  Then BackwardSubstitution[U, ForwardSubstitution[L, IdentityMatrix[Dimensions[M]]]] is the inverse of M."];

Begin["`Private`"]

backwardSubstitutionV = LibraryFunctionLoad[libQMMLPack, "qmml_backward_substitution_v", {{Real, 2, "Constant"}, {Real, 1, "Constant"}}, {Real, 1}];
backwardSubstitutionM = LibraryFunctionLoad[libQMMLPack, "qmml_backward_substitution_m", {{Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}];

BackwardSubstitution[{{}}, {}] = {};
BackwardSubstitution[{{}}, {{}}] = {{}};

BackwardSubstitution[U_?(MatrixQ[#, NumericQ] &), b_?(VectorQ[#, NumericQ] &)] := backwardSubstitutionV[U, b];
BackwardSubstitution[U_?(MatrixQ[#, NumericQ] &), B_?(MatrixQ[#, NumericQ] &)] := backwardSubstitutionM[U, B];

End[]


(*  ******************************  *)
(*  *  ForwardBackwardSubstitution  *)
(*  ******************************  *)

If[Not[ValueQ[ForwardBackwardSubstitution::usage]], ForwardBackwardSubstitution::usage =
"ForwardBackwardSubstitution[L,b] solves L.L\[Transpose].x = b for lower triangular matrix L and vector b."];
(* ForwardBackwardSubstitution[L,B] solves L.L\[Transpose].X = B for matrix B."]; *)

Begin["`Private`"]

forwardBackwardSubstitutionV = LibraryFunctionLoad[libQMMLPack, "qmml_forward_backward_substitution_v", {{Real, 2, "Constant"}, {Real, 1, "Constant"}}, {Real, 1}];
(* forwardBackwardSubstitutionM = LibraryFunctionLoad[libQMMLPack, "qmml_forward_backward_substitution_m", {{Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}]; *)

ForwardBackwardSubstitution[{{}}, {}] = {};
(* ForwardBackwardSubstitution[{{}}, {{}}] = {{}}; *)

ForwardBackwardSubstitution[L_?(MatrixQ[#, NumericQ] &), b_?(VectorQ[#, NumericQ] &)] := forwardBackwardSubstitutionV[L, b];
(* ForwardBackwardSubstitution[L_?(MatrixQ[#, NumericQ] &), B_?(MatrixQ[#, NumericQ] &)] := forwardBackwardSubstitutionM[U, B]; *)

End[]


(*  *********************************  *)
(*  *  BackwardForwardSubstitution  *  *)
(*  *********************************  *)

If[Not[ValueQ[BackwardForwardSubstitution::usage]], BackwardForwardSubstitution::usage =
"BackwardForwardSubstitution[U,b] solves U\[Transpose].U.x = b for upper triangular matrix U and vector b."];
(* BackwardForwardSubstitution[U,B] solves U\[Transpose].U.X = B for matrix B."]; *)

Begin["`Private`"]

backwardForwardSubstitutionV = LibraryFunctionLoad[libQMMLPack, "qmml_backward_forward_substitution_v", {{Real, 2, "Constant"}, {Real, 1, "Constant"}}, {Real, 1}];
(* backwardForwardSubstitutionM = LibraryFunctionLoad[libQMMLPack, "qmml_backward_forward_substitution_m", {{Real, 2, "Constant"}, {Real, 2, "Constant"}}, {Real, 2}]; *)

BackwardForwardSubstitution[{{}}, {}] = {};
(* BackwardForwardSubstitution[{{}}, {{}}] = {{}}; *)

BackwardForwardSubstitution[U_?(MatrixQ[#, NumericQ] &), b_?(VectorQ[#, NumericQ] &)] := backwardForwardSubstitutionV[U, b];
(* BackwardForwardSubstitution[U_?(MatrixQ[#, NumericQ] &), B_?(MatrixQ[#, NumericQ] &)] := backwardForwardSubstitutionM[U, B]; *)

End[]


EndPackage[]
