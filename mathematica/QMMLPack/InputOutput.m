(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(* Import and export formats *)

BeginPackage["QMMLPack`", {"LibraryLink`"}]


(*  ***************  *)
(*  *  Utilities  *  *)
(*  ***************  *)

Begin["`Private`"]

(* Formatting helper for integers, reals, vectors, matrices *)
exportFormat[arg_, ndigits_:Automatic] := Module[{digits, t, nr = 8, ni = 10},
    digits = If[ndigits === Automatic, Ceiling[Log[10, Floor[Max[Abs[arg]]]+1]], ndigits]; (* If arg is not numeric, ndigits is obligatory *)

    Which[
        NumericQ[arg], ToString[NumberForm[arg, {nr+digits,nr}, NumberPadding->{" ","0"}, NumberSigns->{"-"," "}, ExponentFunction->(Null&)]],
        VectorQ[arg, IntegerQ], Map[IntegerString[#, ni, digits]&, arg],
        VectorQ[arg, NumericQ], Map[ToString[NumberForm[#, {nr+digits,nr}, NumberPadding->{" ","0"}, NumberSigns->{"-"," "}, ExponentFunction->(Null&)]]&, arg],
        MatrixQ[arg, IntegerQ], Map[IntegerString[#, ni, digits]&, arg, {2}],
        MatrixQ[arg, NumericQ], Map[ToString[NumberForm[#, {nr+digits,nr}, NumberPadding->{" ","0"}, NumberSigns->{"-"," "}, ExponentFunction->(Null&)]]&, arg, {2}],
        True,
            Map[(
                t = If[StringQ[#], #, ToString[#, CForm]];
                t <> StringJoin[ConstantArray[" ", Max[0, digits-StringLength[t]]]]
            ) &, arg, {2}]
    ]
];

(* Regular expression for number strings *)
(* Includes scientific notation 1.23e-45; use Internal'StringToDouble to convert as ToExpression fails for these *)
reNumber = "[+-]?[0-9]+(\\.[0-9]+)?([eE][+-]?[0-9]+)?"; creNumber = RegularExpression[reNumber];

(* Atomic numbers to avoid many slow calls to ElementData and associated bug (Mathematica sometimes closes the ElementData stream when exporting) *)
elementAbbreviations = {"H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs","Mt","Ds","Rg","Cn","Uut","Fl","Uup","Lv","Uus","Uuo"};
Do[elementData[i, "Abbreviation"] = elementAbbreviations[[i]]; elementData[elementAbbreviations[[i]], "AtomicNumber"] = i;, {i, Length[elementAbbreviations]}];

End[]


(*  *******************************  *)
(*  *  File format: extended XYZ  *  *)
(*  *******************************  *)

Begin["`Private`"]

(* See here for more information: *)
(* https://camtools.cam.ac.uk/wiki/site/5b59f819-0806-4a4d-0046-bcad6b9ac70f/extendedxyz.html *)
(* http://jrkermode.co.uk/quippy/io.html#extendedxyz *)
(* https://dirac.ruc.dk/wiki/index.php/Extended_xyz_format *)

formatsExtendedXYZ1 = LibraryFunctionCheckedLoad[libQMMLPack, "qmml_import_extxyz1", {"UTF8String", "Boolean"}, "UTF8String"]; (* From file *)
formatsExtendedXYZ2 = LibraryFunctionCheckedLoad[libQMMLPack, "qmml_import_extxyz2", {"UTF8String", "Boolean"}, "UTF8String"]; (* From string *)

(* Import *)

ExtendedXYZImportElements = {
    {"VertexTypes", ExtendedXYZImportVertexTypes},                   (* Atom types *)
    {"VertexCoordinates", ExtendedXYZImportVertexCoordinates},       (* Atom coordinates *)
    {"MolecularProperties", ExtendedXYZImportMolecularProperties},   (* Molecular properties *)
    {"AtomicProperties", ExtendedXYZImportAtomicProperties},         (* Atomic properties *)
    {"AdditionalProperties", ExtendedXYZImportAdditionalProperties}  (* Additional properties *)
};

(* Default importer loads and parses complete file *)

ExtendedXYZImportDefault::fail = "Parsing of extended XYZ format failed: `1`";

Options[ExtendedXYZImportDefault] = {"AdditionalProperties" -> False};

(*ExtendedXYZImportDefault[s_String, opts___?OptionQ] := Module[{oaddprop},
    oaddprop = OptionValue[ExtendedXYZImportDefault, opts, "AdditionalProperties"];
    Assert[MemberQ[{False, True}, oaddprop]];

    ToExpression[formatsExtendedXYZ1[s, oaddprop]]
];*)

ExtendedXYZImportDefault[filename_String, opts___?OptionQ] := Module[{oaddprop,parse,res},
    oaddprop = OptionValue[ExtendedXYZImportDefault, opts, "AdditionalProperties"];
    Assert[MemberQ[{False, True}, oaddprop]];

    parse = formatsExtendedXYZ1[filename, oaddprop];
    res = ToExpression[parse];
    If[res === $Failed, Message[ExtendedXYZImportDefault::fail, parse]];

    res
];

(* Export *)

Options[ExtendedXYZExport] = {"Names" -> None, "MolecularProperties" -> {}, "AtomicProperties" -> {}};

(* Data is atomic numbers and coordinates *)
(* Atomic and molecular properties are given as {{p1m1,p1m2,...},{p2m1,p2m2,...},...}, i.e., first index is property, second index is molecule.
   For atomic properties, third index is atom *)
ExtendedXYZExport[filename_String, data_, opts___?OptionQ] := Module[{an,xyz,stream,name,mproperties,aproperties,sout,i,j,nmols,n,block,t},
    (* Parse data *)
    If[Not[ListQ[data]] || Length[data] != 2, Message[Import::fmterr, "extXYZ"]; Return[$Failed]];
    {an,xyz} = If[MemberQ[{Rule, RuleDelayed}, Head[First[data]]], {"VertexTypes" /. data, "VertexCoordinates" /. data}, data];

    (* Options *)
    {name,mproperties,aproperties} = OptionValue[ExtendedXYZExport, {opts}, {"Names", "MolecularProperties", "AtomicProperties"}];

    (* Treat single molecule as a special case of multiple molecules *)
    If[Depth[an] == 2, (
        nmols = 1;
        {an,xyz} = {{an},{xyz}};
        name = {name};
        mproperties = {mproperties};
        aproperties = {aproperties};
    ),(
        nmols = Length[an];
        If[name === None, name = ConstantArray[None, nmols];];
        If[mproperties === {}, mproperties = ConstantArray[{}, nmols];];
        If[aproperties === {}, aproperties = Table[{}, {i,nmols}, {j,Length[an[[i]]]}];];
    )];

    (* Convert atomic numbers from proton numbers to element abbreviations if necessary; no formatting yet *)
    an = Map[If[IntegerQ[#], elementData[#, "Abbreviation"], #] &, an, {2}];

    (* Create extended XYZ file *)
    sout = "";
    For[i = 1, i <= nmols, ++i, (
        (* Line 1: Number of atoms *)
        n = Length[an[[i]]];
        sout = sout <> IntegerString[n]<>"\n";

        (* Line 2: Molecular properties *)
        sout = sout <> If[name[[i]] === None, "", name[[i]] <> " "]; (* Molecule name if present *)
        t = Map[exportFormat[#] &, mproperties[[i]]]; (* exportFormat takes care of formatting numbers correctly; ToString would mess up with, e.g., large numbers 1.2*^6 *)
        (* t = Map[ToString, mproperties[[All,i]], {-1}]; t = Map[If[VectorQ[#], StringJoin[Riffle[#, " "]], #] &, t]; sout = sout <> StringJoin[Riffle[t, "  "]] <> "\n"; *)
        sout = sout <> StringJoin[Riffle[t, " "]] <> "\n";

        (* Line 3ff: molecule and atomic properties *)
        block = Join[{Transpose[{an[[i]]}], xyz[[i]]}, {aproperties[[i]]}]; (* an, xyz, ap; each a matrix *)
        block = Map[exportFormat[#, If[MemberQ[#, _?(Not[NumericQ[#]]&), {2}], 3, Automatic]] &, block];  (* format each entry *)
        (* block = Map[If[MatrixQ[#], #, {#}] &, block]; *)
        block = Map[StringJoin[Riffle[#, " "]] &, block, {2}];
        block = ReleaseHold[Riffle[block, Hold[Transpose[{ConstantArray["   ", n]}]]]]; (* Intersperse major blocks with more spacing *)
        block = Map[StringJoin, Transpose[block]];
        block = StringJoin[Riffle[block, "\n"]];
        sout = sout <> block;

        (* No empty line if no additional properties *)
        If[i < nmols, sout = sout <> "\n"];
    )];

    stream = OpenWrite[filename];
    WriteString[stream, sout];
    Close[stream];
];

End[]

(*ImportExport`RegisterImport[
    "extXYZ",
    Append[QMMLPack`Private`ExtendedXYZImportElements[[All,2]], QMMLPack`Private`ExtendedXYZImportDefault],
    "AvailableElements" -> QMMLPack`Private`ExtendedXYZImportElements[[All,1]],
    "FunctionChannels" -> {"Streams"}
];*)

ImportExport`RegisterImport[
    "extXYZ",
    Append[MapThread[#1 :> #2 &, Transpose[QMMLPack`Private`ExtendedXYZImportElements]], QMMLPack`Private`ExtendedXYZImportDefault],
    "AvailableElements" -> QMMLPack`Private`ExtendedXYZImportElements[[All,1]]
];

ImportExport`RegisterExport[
    "extXYZ",
    QMMLPack`Private`ExtendedXYZExport,
    "Options" -> {"Name", "MolecularProperties", "AtomicProperties"}
];


EndPackage[]
