// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Utility routines for testing

#ifndef QMMLPACK_TESTS_HPP_INCLUDED  // include guard
#define QMMLPACK_TESTS_HPP_INCLUDED

#include <string>
#include <cstdlib>
#include <stdexcept>
#include <algorithm>

//  //////////////////////
//  //  Temporary file  //
//  //////////////////////

// Creates a RAII temporary file.
// * File exists as long as object does
class temporary_file
{
public:
    temporary_file(std::string const& content = "")
    {
        // determine filename_
        char const * const try1 = std::getenv("TMPDIR");
        if(try1 != nullptr) filename_ = try1; else throw std::runtime_error("Environment variable TMPDIR not defined.");
        filename_ = filename_ + random_string(10);

        // create the file, write content if requested
        std::FILE * f = std::fopen(filename_.c_str(), "wb+");
        if(!f) throw std::runtime_error("Temporary file creation failed.");
        const int rv = std::fputs(content.c_str(), f);
        if(rv == EOF) throw std::runtime_error("Temporary file write failed.");
        std::fclose(f); f = nullptr;
    }

    ~temporary_file()
    {
        const int rv = std::remove(filename_.c_str());
        if(rv) throw std::runtime_error("Temporary file deletion failed.");
    }

    std::string const& filename() const { return filename_; }

protected:
    // returns random alphanumerical sequence of given length
    std::string random_string(size_t length)
    {
        static const char charset[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        static const size_t max_index = (sizeof(charset) - 1);
        auto randchar = []() -> char { return charset[ rand() % max_index ]; };

        std::string res(length,0);
        std::generate_n(res.begin(), length, randchar);
        return res;
    }

    std::string filename_;
};

//  //////////////////////
//  //  Random numbers  //
//  //////////////////////

// Returns random number in the interval [lower,upper)
double random_uniform_real(double lower, double upper);

//  ///////////////////////////////////////////
//  //  Comparison of floating point values  //
//  ///////////////////////////////////////////

// Convenient comparison of floating point vectors and matrices (contiguous memory areas)
template<typename T> bool equal_abs(T const*const lhs, T const*const rhs, const size_t n)
{
    for(size_t i = 0; i < n; ++i)
    {
        if( lhs[i] != Approx(rhs[i]) ) return false;
    }
    return true;
}

//  ////////////////////////////////////////
//  //  Test whether matrix is symmetric  //
//  ////////////////////////////////////////

// True if matrix is exactly symmetric (all bits)
template<typename T> bool is_symmetric_matrix(T const*const m, const size_t n)
{
    for(size_t i = 0; i < n-1; ++i)
        for(size_t j = i+1; j < n; ++j)
            if( m[i*n+j] != m[j*n+i] ) return false;
    return true;
}

#endif  // include guard
