// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Unit tests for regression

#include "catch/catch.hpp"
#include "qmmlpack/numerics.hpp"
#include "util.hpp"

using namespace qmml;


//  /////////////////////////////
//  //  Lower triangular part  //
//  /////////////////////////////

TEST_CASE( "lower_triangular_part[_nelems]", "[numerics]" )
{
    // factored out test body
    auto test_body = [](double const*const M, const size_t n, const size_t m, double const*const r, const size_t p, const ptrdiff_t k)
    {
        double v[p]; size_t q;

        q = lower_triangular_part_nelems(n, m, k);
        lower_triangular_part(&v[0], M, n, m, k);

        CHECK( q == p );
        CHECK( equal_abs( v, r, p ) );
    };

    SECTION( "3 x 3 matrices" )
    {
        const size_t n = 3, m = 3;
        const double M[n][m] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

        // k = 0 (default)
        {
            const size_t p = 6; size_t q;
            const double r[p] { 1, 4, 5, 7, 8, 9 }; double v[p];

            q = lower_triangular_part_nelems(n, m);
            lower_triangular_part(&v[0], &M[0][0], n, m);

            CHECK( q == p );
            CHECK( equal_abs( v, r, p ) );
        }

        // k = 0 (explicit)
        {
            const double r[] { 1, 4, 5, 7, 8, 9 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 0);
        }

        // k = 1
        {
            const double r[8] { 1, 2, 4, 5, 6, 7, 8, 9 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 1);
        }

        // k = 2
        {
            const double r[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 2);
        }

        // k = 3
        {
            const double r[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 3);
        }

        // k = 99
        {
            const double r[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 99);
        }

        // k = -1
        {
            const double r[] { 4, 7, 8 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), -1);
        }

        // k = -2
        {
            const double r[] { 7 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), -2);
        }

        // k = -3
        {
            const double r[] { };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), -3);
        }

        // k = -99
        {
            const double r[] { };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), -99);
        }
    }

    SECTION( "4 x 3 matrices" )
    {
        const size_t n = 4, m = 3;
        const double M[n][m] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 10, 11, 12 } };

        // k = 0 (default)
        {
            const size_t p = 9; size_t q;
            const double r[p] { 1, 4, 5, 7, 8, 9, 10, 11, 12 }; double v[p];

            q = lower_triangular_part_nelems(n, m);
            lower_triangular_part(&v[0], &M[0][0], n, m);

            CHECK( q == p );
            CHECK( equal_abs( v, r, p ) );
        }

        // k = 0 (explicit)
        {
            const double r[] { 1, 4, 5, 7, 8, 9, 10, 11, 12 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 0);
        }

        // k = -1
        {
            const double r[] { 4, 7, 8, 10, 11, 12 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), -1);
        }

        // k = -2
        {
            const double r[] { 7, 10, 11 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), -2);
        }

        // k = -3
        {
            const double r[] { 10 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), -3);
        }

        // k = -4
        {
            const double r[] { };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), -4);
        }

        // k = -99
        {
            const double r[] { };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), -99);
        }

        // k = 1
        {
            const double r[] { 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 1);
        }

        // k = 2
        {
            const double r[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 2);
        }

        // k = 3
        {
            const double r[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 3);
        }

        // k = 99
        {
            const double r[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            test_body(&M[0][0], n, m, &r[0], (sizeof(r)/sizeof(*r)), 99);
        }
    }
}


//  //////////////////
//  //  Symmetrize  //
//  //////////////////

TEST_CASE("symmetrize", "[numerics]")
{
    SECTION( "special cases" )
    {
        // empty matrix
        {
            symmetrize(nullptr, 0);
        }

        // 1x1 matrix
        {
            double M[1][1] { { 1 } };
            const double r[1][1] { { 1 } };
            symmetrize(&M[0][0], 1);
            CHECK( equal_abs(&M[0][0], &r[0][0], 1*1) );
        }

        // 2x2 matrix, unchanged
        {
            double M[2][2] { { 1, 3 }, { 3, 5 } };
            const double r[2][2] { { 1, 3 }, { 3, 5 } };
            symmetrize(&M[0][0], 2);
            CHECK( equal_abs(&M[0][0], &r[0][0], 2*2) );
        }

        // 2x2 matrix, averaging
        {
            double M[2][2] { { 1, 2 }, { 4, 5 } };
            const double r[2][2] { { 1, 3 }, { 3, 5 } };
            symmetrize(&M[0][0], 2);
            CHECK( equal_abs(&M[0][0], &r[0][0], 2*2) );
        }
    }

    SECTION( "randomized tests" )
    {
        // 100 x 100 matrix
        {
            const size_t n = 100;
            double M[n][n];
            for(size_t i = 0; i < n; ++i) for(size_t j = 0; j < n; ++j) M[i][j] = random_uniform_real(-1, 1);
            symmetrize(&M[0][0], n);
            CHECK( is_symmetric_matrix(&M[0][0], n) );
        }
    }
}


//  /////////////////////////////////////////
//  //  Forward and backward substitution  //
//  /////////////////////////////////////////

TEST_CASE("forward_substitution_v", "[numerics][regression]")
{
    SECTION( "special cases" )
    {
        // x = b
        {
            const double L[3][3] { { 1, 0, 0 }, { 2, 3, 0 }, { 4, 5, 6 } };
            const double r[3] { 1, 2, 3 };
            double x[3] { 1, 8, 32 };

            forward_substitution_v(&x[0], &L[0][0], &x[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }
    }

    SECTION( "simple examples" )
    {
        // n = 1
        {
            const double L[1][1] { { 1 } };
            const double b[1] { 1 };
            const double r[1] { 1 };
            double x[1];

            forward_substitution_v(&x[0], &L[0][0], &b[0], 1);
            CHECK( equal_abs(&x[0], &r[0], 1) );
        }

        // n = 3, diagonal
        {
            const double L[3][3] { { 1, 0, 0 }, { 0, 2, 0 }, { 0, 0, 3 } };
            const double b[3] { 1, 2, 3 };
            const double r[3] { 1, 1, 1 };
            double x[3];

            forward_substitution_v(&x[0], &L[0][0], &b[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }

        // n = 3, non-diagonal
        {
            const double L[3][3] { { 1, 0, 0 }, { 2, 3, 0 }, { 4, 5, 6 } };
            const double b[3] { 1, 8, 32 };
            const double r[3] { 1, 2, 3 };
            double x[3];

            forward_substitution_v(&x[0], &L[0][0], &b[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }
    }
}

TEST_CASE("forward_substitution_m", "[numerics][regression]")
{
    SECTION( "special cases" )
    {
        // X = B
        {
            const double L[3][3] { { 1,  0, 0 }, { 2,  3,  0 }, {  4,   5,  6 } };
            const double R[3][4] { { 1, -1, 2, -2 }, { 2, -2,  4, - 4 }, {  3, - 3,  8, - 8 } };
            double X[3][4] { { 1, -1, 2, -2 }, { 8, -8, 16, -16 }, { 32, -32, 76, -76 } };

            forward_substitution_m(&X[0][0], &L[0][0], &X[0][0], 3, 4);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*4) );
        }
    }

    SECTION( "simple examples" )
    {
        // n = 1, m = 1
        {
            const double L[1][1] { { 1 } };
            const double B[1][1] { { 1 } };
            const double R[1][1] { { 1 } };
            double X[1][1];

            forward_substitution_m(&X[0][0], &L[0][0], &B[0][0], 1, 1);
            CHECK( equal_abs(&X[0][0], &R[0][0], 1*1) );
        }

        // n = 3, m = 3, diagonal
        {
            const double L[3][3] { { 1, 0, 0 }, { 0, 2, 0 }, { 0, 0, 3 } };
            const double B[3][3] { { 1, 1, 1 }, { 2, 2, 2 }, { 3, 3, 3 } };
            const double R[3][3] { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
            double X[3][3];

            forward_substitution_m(&X[0][0], &L[0][0], &B[0][0], 3, 3);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*3) );
        }

        // n = 3, m = 3, non-diagonal
        {
            const double L[3][3] { { 1,  0, 0 }, { 2,  3,  0 }, {  4,   5,  6 } };
            const double B[3][3] { { 1, -1, 2 }, { 8, -8, 16 }, { 32, -32, 76 } };
            const double R[3][3] { { 1, -1, 2 }, { 2, -2,  4 }, {  3, - 3,  8 } };
            double X[3][3];

            forward_substitution_m(&X[0][0], &L[0][0], &B[0][0], 3, 3);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*3) );
        }

        // n = 3, m = 2, non-diagonal
        {
            const double L[3][3] { { 1,  0, 0 }, { 2,  3,  0 }, {  4,   5,  6 } };
            const double B[3][2] { { 1, -1 }, { 8, -8 }, { 32, -32 } };
            const double R[3][2] { { 1, -1 }, { 2, -2 }, {  3, - 3 } };
            double X[3][2];

            forward_substitution_m(&X[0][0], &L[0][0], &B[0][0], 3, 2);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*2) );
        }

        // n = 3, m = 4
        {
            const double L[3][3] { { 1,  0, 0 }, { 2,  3,  0 }, {  4,   5,  6 } };
            const double B[3][4] { { 1, -1, 2, -2 }, { 8, -8, 16, -16 }, { 32, -32, 76, -76 } };
            const double R[3][4] { { 1, -1, 2, -2 }, { 2, -2,  4, - 4 }, {  3, - 3,  8, - 8 } };
            double X[3][4];

            forward_substitution_m(&X[0][0], &L[0][0], &B[0][0], 3, 4);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*4) );
        }
    }
}

TEST_CASE("backward_substitution_v", "[numerics][regression]")
{
    SECTION( "special cases" )
    {
        // x = b
        {
            const double U[3][3] { { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } };
            const double r[3] { 3, 2, 1 };
            double x[3] { 32, 8, 1 };

            backward_substitution_v(&x[0], &U[0][0], &x[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }
    }

    SECTION( "simple examples" )
    {
        // n = 1
        {
            const double L[1][1] { { 1 } };
            const double b[1] { 1 };
            const double r[1] { 1 };
            double x[1];

            backward_substitution_v(&x[0], &L[0][0], &b[0], 1);
            CHECK( equal_abs(&x[0], &r[0], 1) );
        }

        // n = 3, diagonal
        {
            const double U[3][3] { { 3, 0, 0 }, { 0, 2, 0 }, { 0, 0, 1 } };
            const double b[3] { 3, 2, 1 };
            const double r[3] { 1, 1, 1 };
            double x[3];

            backward_substitution_v(&x[0], &U[0][0], &b[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }

        // n = 3, non-diagonal
        {
            const double U[3][3] { { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } };
            const double b[3] { 32, 8, 1 };
            const double r[3] { 3, 2, 1 };
            double x[3];

            backward_substitution_v(&x[0], &U[0][0], &b[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }
    }
}

TEST_CASE("backward_substitution_m", "[numerics][regression]")
{
    SECTION( "special cases" )
    {
        {
            const double U[3][3] { { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } };
            const double R[3][4] { {  3, - 3,  8, - 8 }, { 2, -2,  4, - 4 }, { 1, -1, 2, -2 } };
            double X[3][4] { { 32, -32, 76, -76 }, { 8, -8, 16, -16 }, { 1, -1, 2, -2 } };

            backward_substitution_m(&X[0][0], &U[0][0], &X[0][0], 3, 4);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*4) );
        }
    }

    SECTION( "simple examples" )
    {
        // n = 1, m = 1
        {
            const double U[1][1] { { 1 } };
            const double B[1][1] { { 1 } };
            const double R[1][1] { { 1 } };
            double X[1][1];

            backward_substitution_m(&X[0][0], &U[0][0], &B[0][0], 1, 1);
            CHECK( equal_abs(&X[0][0], &R[0][0], 1*1) );
        }

        // n = 3, m = 3, diagonal
        {
            const double U[3][3] { { 3, 0, 0 }, { 0, 2, 0 }, { 0, 0, 1 } };
            const double B[3][3] { { 3, 3, 3 }, { 2, 2, 2 }, { 1, 1, 1 } };
            const double R[3][3] { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
            double X[3][3];

            backward_substitution_m(&X[0][0], &U[0][0], &B[0][0], 3, 3);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*3) );
        }

        // n = 3, m = 3, non-diagonal
        {
            const double U[3][3] { {  6,   5,  4 }, { 0,  3,  2 }, { 0,  0, 1 } };
            const double B[3][3] { { 32, -32, 76 }, { 8, -8, 16 }, { 1, -1, 2 } };
            const double R[3][3] { {  3,  -3,  8 }, { 2, -2,  4 }, { 1, -1, 2 } };
            double X[3][3];

            backward_substitution_m(&X[0][0], &U[0][0], &B[0][0], 3, 3);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*3) );
        }

        // n = 3, m = 2, non-diagonal
        {
            const double U[3][3] { { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } };
            const double B[3][2] { { 32, -32 }, { 8, -8 }, { 1, -1 } };
            const double R[3][2] { {  3, - 3 }, { 2, -2 }, { 1, -1 } };
            double X[3][2];

            backward_substitution_m(&X[0][0], &U[0][0], &B[0][0], 3, 2);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*2) );
        }

        // n = 3, m = 4
        {
            const double U[3][3] { { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } };
            const double B[3][4] { { 32, -32, 76, -76 }, { 8, -8, 16, -16 }, { 1, -1, 2, -2 } };
            const double R[3][4] { {  3, - 3,  8, - 8 }, { 2, -2,  4, - 4 }, { 1, -1, 2, -2 } };
            double X[3][4];

            backward_substitution_m(&X[0][0], &U[0][0], &B[0][0], 3, 4);
            CHECK( equal_abs(&X[0][0], &R[0][0], 3*4) );
        }
    }
}

TEST_CASE("forward_backward_substitution_v", "[numerics][regression]")
{
    SECTION( "special cases" )
    {
        // x = b
        {
            const double L[3][3] { { 1, 0, 0 }, { 2, 3, 0 }, { 4, 5, 6 } };
            const double r[3] { -2/3., -1/6., 1/2. };
            double x[3] { 1, 8, 32 };

            forward_backward_substitution_v(&x[0], &L[0][0], &x[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }
    }

    SECTION( "simple examples" )
    {
        // n = 1
        {
            const double L[1][1] { { 1 } };
            const double b[1] { 1 };
            const double r[1] { 1 };
            double x[1];

            forward_backward_substitution_v(&x[0], &L[0][0], &b[0], 1);
            CHECK( equal_abs(&x[0], &r[0], 1) );
        }

        // n = 3, diagonal
        {
            const double L[3][3] { { 1, 0, 0 }, { 0, 2, 0 }, { 0, 0, 3 } };
            const double b[3] { 1, 2, 3 };
            const double r[3] { 1., 1./2, 1./3 };
            double x[3];

            forward_backward_substitution_v(&x[0], &L[0][0], &b[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }

        // n = 3, non-diagonal
        {
            const double L[3][3] { { 1, 0, 0 }, { 2, 3, 0 }, { 4, 5, 6 } };
            const double b[3] { 1, 8, 32 };
            const double r[3] { -2/3., -1/6., 1/2. };
            double x[3];

            forward_backward_substitution_v(&x[0], &L[0][0], &b[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }
    }
}

TEST_CASE("backward_forward_substitution_v", "[numerics][regression]")
{
    SECTION( "special cases" )
    {
        // x = b
        {
            const double U[3][3] { { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } };
            const double r[3] { 283/81., 86/27., -71/9. };
            double x[3] { 32, 8, 1 };

            backward_forward_substitution_v(&x[0], &U[0][0], &x[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }
    }

    SECTION( "simple examples" )
    {
        // n = 1
        {
            const double U[1][1] { { 1 } };
            const double b[1] { 1 };
            const double r[1] { 1 };
            double x[1];

            backward_forward_substitution_v(&x[0], &U[0][0], &b[0], 1);
            CHECK( equal_abs(&x[0], &r[0], 1) );
        }

        // n = 3, diagonal
        {
            const double U[3][3] { { 3, 0, 0 }, { 0, 2, 0 }, { 0, 0, 1 } };
            const double b[3] { 3, 2, 1 };
            const double r[3] { 1/3., 1/2., 1. };
            double x[3];

            backward_forward_substitution_v(&x[0], &U[0][0], &b[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }

        // n = 3, non-diagonal
        {
            const double U[3][3] { { 6, 5, 4 }, { 0, 3, 2 }, { 0, 0, 1 } };
            const double b[3] { 32, 8, 1 };
            const double r[3] { 283/81., 86/27., -71/9. };
            double x[3];

            backward_forward_substitution_v(&x[0], &U[0][0], &b[0], 3);
            CHECK( equal_abs(&x[0], &r[0], 3) );
        }
    }
}
