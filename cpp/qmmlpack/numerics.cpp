// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Numerics utilities

#include <cstring>
#include "qmmlpack/base.hpp"
#include "qmmlpack/numerics.hpp"

namespace qmml {

//  /////////////////////////////
//  //  Lower triangular part  //
//  /////////////////////////////

// Number of elements in the lower triangular part of an n x m matrix starting from k-th diagonal. Matrix does not have to be quadratic.
size_t lower_triangular_part_nelems(size_t nn, size_t mm, ptrdiff_t k)
{
    // Case 1:  k <= -n
    //   0
    // Case 2: -n < k <= 0
    //   sum_{i=1-k}^n min(i+k,m)
    //   Case 2a: n+k <= m
    //     sum_{i=1-k}^n i+k = (n+k)(n+k+1)/2
    //   Case 2b: n+k > m
    //     sum_{i=1-k}^{m-k} i+k + \sum_{i=m-k+1}^n m = m(1-m+2n+2k)/2
    // Case 3: 0 < k < m
    //   sum_{i=1}^n min(m,k+i)
    //   Case 3a: n+k <= m
    //     sum_{i=1}^n k+i = n(2k+n+1)/2
    //   Case 3b: n+k > m
    //     sum_{i=1}^{m-k} k+i + sum_{i=m-k+1}^n m = (m-k)(k+1-m)/2 + mn
    // Case 4: m <= k
    //   n*m

    const ptrdiff_t n = nn;  // need unsigned expressions involving n
    const ptrdiff_t m = mm;  // need unsigned expressions involving m

    size_t len;
    if(k <= -n) len = 0;
    else if(k <= 0) len = (n+k <= m) ? ((n+k)*(n+k+1))/2 : (m*(1-m+2*n+2*k))/2;
    else if(k <  m) len = (n+k <= m) ? (n*(2*k+n+1))/2   : ((m-k)*(k+1-m))/2+m*n;
    else len = n*m; // k >= m

    return len;
}

// Vector containing lower triangular part of n x m matrix starting from k-th diagonal
void lower_triangular_part(double *const v, double const*const M, const size_t nn, const size_t mm, ptrdiff_t k)
{
    const ptrdiff_t n = nn;  // need unsigned expressions involving n
    const ptrdiff_t m = mm;  // need unsigned expressions involving n

    if(k <= -n)
    {
        // Zero length. Do nothing.
    }
    else if(k <= 0)
    {
        if(n+k <= m)
        {
            size_t t = 0;
            for(size_t i = -k; i < n; ++i) for(size_t j = 0; j <= i+k; ++j) v[t++] = M[i*m+j];
        }
        else
        {
            size_t t = 0;
            for(size_t i = -k; i < m-k; ++i) for(size_t j = 0; j <= i+k; ++j) v[t++] = M[i*m+j];
            std::memcpy(&v[t], &M[(m-k)*m+0], (n-m+k)*m*sizeof(double));
        }
    }
    else if(k < m)
    {
        if(n+k <= m)
        {
            size_t t = 0;
            for(size_t i = 0; i < n; ++i) for(size_t j = 0; j <= i+k; ++j) v[t++] = M[i*m+j];
        }
        else
        {
            size_t t = 0;
            for(size_t i = 0; i < m-k; ++i) for(size_t j = 0; j <= i+k; ++j) v[t++] = M[i*m+j];
            std::memcpy(&v[t], &M[(m-k)*m+0], (n-m+k)*m*sizeof(double));
        }
    }
    else
    {
        std::memcpy(&v[0], &M[0], n*m*sizeof(double));
    }
}


//  //////////////////
//  //  Symmetrize  //
//  //////////////////

void symmetrize(double *const M, const size_t n)
{
    // omit diagonal
    for(size_t i = 0; i+1 < n; ++i)
        for(size_t j = i+1; j < n; ++j)
        {
            const double val = ( M[i*n+j] + M[j*n+i] ) / 2;
            M[i*n+j] = M[j*n+i] = val;
        }
}


//  /////////////////////////////////////////
//  //  Forward and backward substitution  //
//  /////////////////////////////////////////

void forward_substitution_v(double *const x, double const*const L, double const*const b, const size_t n)
{
    if(x != b) std::memcpy(x, b, n*sizeof(double));
    // cblas_dtrsv(CblasRowMajor, CblasLower, CblasNoTrans, CblasNonUnit, n, L, n, x, 1);
    cblas_dtrsv(CblasColMajor, CblasUpper, CblasTrans, CblasNonUnit, n, L, n, x, 1);

    // Naive implementation is slower than BLAS version
    // for(size_t i = 0; i < n; ++i) { double val = b[i]; for(size_t j = 0; j < i; ++j) val -= L[i*n+j]*x[j]; x[i] = val / L[i*n+i]; }
}

void forward_substitution_m(double *const X, double const*const L, double const*const B, const size_t n, const size_t m)
{
    if(X != B) std::memcpy(X, B, n*m*sizeof(double));
    // cblas_dtrsm(CblasRowMajor, CblasLeft, CblasLower, CblasNoTrans, CblasNonUnit, n, m, 1., L, n, X, m);
    cblas_dtrsm(CblasColMajor, CblasRight, CblasUpper, CblasNoTrans, CblasNonUnit, m, n, 1., L, n, X, m);
}

void backward_substitution_v(double *const x, double const*const U, double const*const b, const size_t n)
{
    if(x != b) std::memcpy(x, b, n*sizeof(double));
    // cblas_dtrsv(CblasRowMajor, CblasUpper, CblasNoTrans, CblasNonUnit, n, U, n, x, 1);
    cblas_dtrsv(CblasColMajor, CblasLower, CblasTrans, CblasNonUnit, n, U, n, x, 1);
}

void backward_substitution_m(double *const X, double const*const U, double const*const B, const size_t n, const size_t m)
{
    if(X != B) std::memcpy(X, B, n*m*sizeof(double));
    // cblas_dtrsm(CblasRowMajor, CblasLeft, CblasUpper, CblasNoTrans, CblasNonUnit, n, m, 1., U, n, X, m);
    cblas_dtrsm(CblasColMajor, CblasRight, CblasLower, CblasNoTrans, CblasNonUnit, m, n, 1., U, n, X, m);
}

void forward_backward_substitution_v(double *const x, double const*const L, double const*const b, const size_t n)
{
    forward_substitution_v(x, L, b, n);  // L x = b
    // backward_substitution_v(x, L^T, x, n) requires L^T, which is not available
    cblas_dtrsv(CblasColMajor, CblasUpper, CblasNoTrans, CblasNonUnit, n, L, n, x, 1);
}

void backward_forward_substitution_v(double *const x, double const*const U, double const*const b, const size_t n)
{
    // forward_substitution_v(x, U^T, b, n) requires U^T, which is not available
    if(x != b) std::memcpy(x, b, n*sizeof(double));
    cblas_dtrsv(CblasColMajor, CblasLower, CblasNoTrans, CblasNonUnit, n, U, n, x, 1);
    backward_substitution_v(x, U, x, n);
}

} // namespace qmml
