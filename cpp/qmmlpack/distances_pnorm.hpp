// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Distance functions based on p-norms

#ifndef QMMLPACK_DISTANCES_PNORMS_HPP_INCLUDED  // Include guard
#define QMMLPACK_DISTANCES_PNORMS_HPP_INCLUDED

namespace qmml {

//  /////////////////////////
//  //  one-norm distance  //
//  /////////////////////////

// one-norm distance between a set of vectors
// D  contiguous memory block of size n * n  * size(T)
// X  contiguous memory block of size n * d  * size(T)
// n  number of vectors
// d  number of dimensions of vectors
void distance_matrix_one_norm_k (double * const D, double const*const X, const size_t n, const size_t d);

// one-norm distance between two sets of vectors
// D  contiguous memory block of size n * m  * size(T)
// X  contiguous memory block of size n * d  * size(T)
// Z  contiguous memory block of size m * d  * size(T)
// n  number of vectors in set X
// m  number of vectors in set Z
// d  number of dimensions of vectors
void distance_matrix_one_norm_l (double * const D, double const*const X, double const*const Z, const size_t n, const size_t m, const size_t d);


//  //////////////////////////
//  //  Euclidean distance  //
//  //////////////////////////

// See notes for squared Euclidean distance.

// Euclidean distance between a set of vectors
// D  contiguous memory block of size n * n  * size(T)
// X  contiguous memory block of size n * d  * size(T)
// n  number of vectors
// d  number of dimensions of vectors
void distance_matrix_euclidean_k (double * const D, double const*const X, const size_t n, const size_t d);

// Euclidean distance between two sets of vectors
// D  contiguous memory block of size n * m  * size(T)
// X  contiguous memory block of size n * d  * size(T)
// Z  contiguous memory block of size m * d  * size(T)
// n  number of vectors in set X
// m  number of vectors in set Z
// d  number of dimensions of vectors
void distance_matrix_euclidean_l (double * const D, double const*const X, double const*const Z, const size_t n, const size_t m, const size_t d);


//  //////////////////////////////////
//  //  Squared Euclidean distance  //
//  //////////////////////////////////

// Current implemenation uses BLAS/dot product formulation,
// which will have (small) errors in the form of non-zero
// distance for identical samples. Magnitude of errors grows
// with number of dimensions and range of vector component values.
// Use of a cut-off value might be recommended in some situations.

// Squared Euclidean distance between a set of vectors
// D  contiguous memory block of size n * n  * size(T)
// X  contiguous memory block of size n * d  * size(T)
// n  number of vectors
// d  number of dimensions of vectors
void distance_matrix_squared_euclidean_k (double * const D, double const*const X, const size_t n, const size_t d);

// Squared Euclidean distance between two sets of vectors
// D  contiguous memory block of size n * m  * size(T)
// X  contiguous memory block of size n * d  * size(T)
// Z  contiguous memory block of size m * d  * size(T)
// n  number of vectors in set X
// m  number of vectors in set Z
// d  number of dimensions of vectors
void distance_matrix_squared_euclidean_l (double * const D, double const*const X, double const*const Z, const size_t n, const size_t m, const size_t d);

} // namespace qmml

#endif  // Include guard
