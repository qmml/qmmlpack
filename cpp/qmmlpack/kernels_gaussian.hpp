// qmmlpack
// (c) Matthias Rupp, 2006-2017.
// See LICENSE.txt for license.

// Gaussian kernel

#ifndef QMMLPACK_KERNELS_GAUSSIAN_HPP_INCLUDED  // include guard
#define QMMLPACK_KERNELS_GAUSSIAN_HPP_INCLUDED

namespace qmml {

//  ///////////////////////
//  //  Gaussian kernel  //
//  ///////////////////////

// The special case sigma = 0 is not handled because it requires either
// distances to be exactly zero for identical samples or a threshold value
// that depends on dimensionality d and magnitude of x_i values.

// Gaussian kernel, from distance matrix
// L  contiguous memory block of size n * m  * size(T)
// D  contiguous memory block of size n * m  * size(T)
// sigma  length scale, positive real number
void kernel_matrix_gaussian_d(double * L, double const * D, size_t n, size_t m, double sigma);

// Gaussian kernel, kernel matrix K
// K  contiguous memory block of size n * n  * size(T)
// X  contiguous memory block of size n * d  * size(T)
// sigma  length scale, positive real number
void kernel_matrix_gaussian_k(double * K, double const * X, size_t n, size_t d, double sigma);

// Gaussian kernel, kernel matrix L
// L  contiguous memory block of size n * m  * size(T)
// X  contiguous memory block of size n * d  * size(T)
// Z  contiguous memory block of size m * d  * size(T)
// sigma  length scale, positive real number
void kernel_matrix_gaussian_l(double * L, double const * X, double const * Z, size_t n, size_t m, size_t d, double sigma);

// For the Gaussian kernel, diagonal is always one;
// parameters X, d, sigma are therefore unnecessary,
// but are kept to maintain the interface used by all kernel functions.

// Gaussian kernel, kernel vector m
// m  contiguous memory block of size n      * size(T)
// X  contiguous memory block of size n * d  * size(T)
// sigma  length scale, non-negative real number
void kernel_matrix_gaussian_m(double * m, double const *, size_t n, size_t, double);

} // namespace qmml

#endif  // include guard
