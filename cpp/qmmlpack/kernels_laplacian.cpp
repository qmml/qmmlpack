// qmmlpack
// (c) Matthias Rupp, 2006-2017.
// See LICENSE.txt for license.

// Laplacian kernel

#include <cmath>
#include <algorithm>

#include "qmmlpack/base.hpp"
#include "qmmlpack/distances_pnorm.hpp"
#include "qmmlpack/kernels_laplacian.hpp"

namespace qmml {

//  ////////////////////////
//  //  Laplacian kernel  //
//  ////////////////////////

// The special case sigma = 0 is not handled because it requires either
// distances to be exactly zero for identical samples or a threshold value
// that depends on dimensionality d and magnitude of x_i values.

void kernel_matrix_laplacian_d(double * const L, double const * const D, const size_t n, const size_t m, const double sigma)
{
    if( sigma <= 0 ) QMML_ERROR("invalid value for Laplacian kernel length scale sigma");

    double const c = - 1. / sigma;
    for(size_t i = 0; i < n*m; ++i) L[i] = std::exp( c * D[i] );
}

void kernel_matrix_laplacian_k(double * const K, double const * const X, const size_t n, const size_t d, const double sigma)
{
    if( sigma <= 0 ) QMML_ERROR("invalid value for Laplacian kernel length scale sigma");

    distance_matrix_one_norm_k(K, X, n, d);
    kernel_matrix_laplacian_d(K, K, n, n, sigma);
}

void kernel_matrix_laplacian_l(double * const L, double const * const X, double const * const Z, const size_t n, const size_t m, const size_t d, const double sigma)
{
    if( sigma <= 0 ) QMML_ERROR("invalid value for Laplacian kernel length scale sigma");

    distance_matrix_one_norm_l(L, X, Z, n, m, d);
    kernel_matrix_laplacian_d(L, L, n, m, sigma);
}

void kernel_matrix_laplacian_m(double * const m, double const * const, const size_t n, const size_t, const double sigma)
{
    if( sigma <= 0 ) QMML_ERROR("invalid value for Laplacian kernel length scale sigma");

    std::fill_n(&m[0], n, 1.);
}

} // namespace qmml
