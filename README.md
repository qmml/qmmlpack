# qmmlpack&mdash;a quantum mechanics / machine learning package

Package for machine learning models that interpolate between first-principles calculations of atomistic systems.
Efficient C++ code with bindings to Mathematica and Python.

## About

Created by [Matthias Rupp](http://mrupp.info/) in support of research on machine-learning models for quantum-mechanical simulations.

If you are using this software, please cite
> Matthias Rupp: Machine Learning for Quantum Mechanics in a Nutshell, International Journal of Quantum Chemistry, 115(16): 1058–1073, 2015. [DOI](http://dx.doi.org/10.1002/qua.24954)

With contributions by
* Marcel Langer (Fritz Haber Institute of the Max Planck Society, Berlin, Germany)
* Lucas Deecke (Free University of Berlin, Berlin, Germany)
* Haoyan Huo (Peking University, Beijing, China)


## Warning

**:warning: This package comes without any warranties, including possibility of breaking changes without notice.**


## Requirements

Designed for scientific computing, this package requires recent, but not bleeding-edge versions of the following software:

* C++/14 compatible compiler, such as LLVM/Clang&nbsp;3.4 or later; GCC&nbsp;5 or later; Intel icc&nbsp;17.0.4 or later
* Mathematica&nbsp;10.1 or later
* Python&nbsp;3.6 or later, including py.test, NumPy&nbsp;1.11, and SciPy&nbsp;0.17 or later

The packages Python functionality does not require Mathematica.

When compiling with Intel icc, corresponding GCC C++ standard libraries must be version 5 or later.
This means that both Intel and GNU compilers should be available and meet version requirements.

## Installation

Create a local copy of the repository, compile the C++ core routines, then the desired bindings (Python, Mathematica). Optionally, install a system-wide version. Adjust paths accordingly.

    git clone git@gitlab.com:qmml/qmmlpack.git
    ./make -v cpp
    ./make -v python          # independent of Mathematica bindings
    ./make -v mathematica     # independent of Python bindings
    sudo ./make -v install    # optional

Typical paths to adjust include the shell's `PYTHONPATH` environment variable, Python's `sys.path` variable and Mathematica's `$Path` variable. For example:

    export PYTHONPATH="/usr/local/qmmlpack/python:$PYTHONPATH"
    import sys; sys.path.append('/usr/local/qmmlpack/python')
    AppendTo[$Path, "/usr/local/qmmlpack/mathematica"];

Troubleshooting:
* Run the make script with `-v` option and carefully check the output, in particular whether the used compiler, library and Python versions and paths are correct. If not use optional arguments to specify paths (see `./make -h`)
* Check whether all unit tests ran successfully
* If the qmmlpack library can not be found, check whether the `LD_LIBRARY_PATH` environment variable is set correctly


## Development

### Dependencies versions

To use [Anaconda](https://www.anaconda.com/) to test against specific versions of dependencies create dedicated environments, for example:

```
conda create --name qmmlpack-py3.6-mac10.14-mm12.0 "python=3.6" "numpy>=1.11" "scipy>=0.17" pytest
conda activate qmmlpack-py3.6-mac10.14-mm12.0
```

### Increase qmmlpack version number

1. Increase version number at top of `make` script (X.Y.Z = major, minor, patch)
2. Create a corresponding tag via `git tag vX.Y.Z`
3. Push (after all changes for new version) to repository via `git push --tags`
