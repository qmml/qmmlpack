// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for input/output of extended XYZ format

#ifndef QMML_PYTHON_IO_EXTXYZ_HPP_INCLUDED  // include guard
#define QMML_PYTHON_IO_EXTXYZ_HPP_INCLUDED

#include "python.hpp"
#include "qmmlpack/io_extxyz.hpp"

// io_exyz

std::vector<qmml::extxyz_data> _import_extxyz_string(std::string const& s, bool addp = false);
std::vector<qmml::extxyz_data> _import_extxyz_file(std::string const& fn, bool addp = false);

// Python bindings

void init_io_extxyz(py::module &m);

#endif // include guard
