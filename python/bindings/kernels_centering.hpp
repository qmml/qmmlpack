// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for centering of kernel matrices

#ifndef QMML_PYTHON_KERNELS_CENTERING_HPP_INCLUDED  // include guard
#define QMML_PYTHON_KERNELS_CENTERING_HPP_INCLUDED

#include "python.hpp"

// center_kernel_matrix

void _center_kernel_matrix_k(py_cdarray_t kc, py_cdarray_t kk);
void _center_kernel_matrix_l(py_cdarray_t lc, py_cdarray_t kk, py_cdarray_t ll);
void _center_kernel_matrix_m(py_cdarray_t mc, py_cdarray_t kk, py_cdarray_t ll, py_cdarray_t mm);
void _center_kernel_matrix_mdiag(py_cdarray_t mc, py_cdarray_t kk, py_cdarray_t ll, py_cdarray_t mm);

// Python bindings

void init_kernels_centering(py::module &m);

#endif // include guard
