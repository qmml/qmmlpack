// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for numerics utilities

#include "numerics.hpp"
#include "qmmlpack/numerics.hpp"

// lower_triangular_part

size_t _lower_triangular_part_nelems(size_t n, size_t m, ptrdiff_t k)
{
    return qmml::lower_triangular_part_nelems(n, m, k);
}

void _lower_triangular_part(py_cdarray_t v, py_cdarray_t M, ptrdiff_t k)
{
    auto bufv = v.request(), bufM = M.request();

    if(bufv.ndim != 1) throw QMML_ERROR("_lower_triangular_part first argument must be a vector");
    if(bufM.ndim != 2) throw QMML_ERROR("_lower_triangular_part second argument must be a matrix");

    const size_t n = bufM.shape[0];
    const size_t m = bufM.shape[1];

    qmml::lower_triangular_part(static_cast<double*>(bufv.ptr), static_cast<double*>(bufM.ptr), n, m, k);
}

// symmetrize

void _symmetrize(py_cdarray_t M)
{
    auto bufM = M.request();

    if(bufM.format != pybind11::format_descriptor<double>::value) throw QMML_ERROR("_symmetrize argument must be 'double' matrix");
    if(bufM.ndim != 2) throw QMML_ERROR("_symmetrize argument must be a matrix");
    if(bufM.shape[0] != bufM.shape[1]) throw QMML_ERROR("_symmetrize argument must be quadratic");

    const size_t n = bufM.shape[0];

    qmml::symmetrize(static_cast<double*>(bufM.ptr), n);
}

// forward and backward substitution

void _forward_substitution_v(py_cdarray_t x, py_cdarray_t L, py_cdarray_t b)
{
    auto bufx = x.request(), bufL = L.request(), bufb = b.request();

    if(bufx.ndim != 1) throw QMML_ERROR("_forward_substitution_v first argument must be a vector");
    if(bufL.ndim != 2) throw QMML_ERROR("_forward_substitution_v second argument must be a matrix");
    if(bufb.ndim != 1) throw QMML_ERROR("_forward_substitution_v third argument must be a vector");

    const size_t n = bufx.shape[0];  // number of samples

    if(bufL.shape[0] != n || bufL.shape[1] != n) throw QMML_ERROR("_forward_substitution_v: matrix L must have compatible dimensions");
    if(bufb.shape[0] != n) throw QMML_ERROR("_forward_substitution_v: vector b must have compatible dimension");

    qmml::forward_substitution_v(static_cast<double*>(bufx.ptr), static_cast<double*>(bufL.ptr), static_cast<double*>(bufb.ptr), n);
}

void _forward_substitution_m(py_cdarray_t X, py_cdarray_t L, py_cdarray_t B)
{
    auto bufX = X.request(), bufL = L.request(), bufB = B.request();

    if(bufX.ndim != 2) throw QMML_ERROR("_forward_substitution_m first argument must be a matrix");
    if(bufL.ndim != 2) throw QMML_ERROR("_forward_substitution_m second argument must be a matrix");
    if(bufB.ndim != 2) throw QMML_ERROR("_forward_substitution_m third argument must be a matrix");

    const size_t n = bufX.shape[0];
    const size_t m = bufX.shape[1];

    if(bufL.shape[0] != n || bufL.shape[1] != n) throw QMML_ERROR("_forward_substitution_m: matrix L must have compatible dimensions");
    if(bufB.shape[0] != n || bufB.shape[1] != m) throw QMML_ERROR("_forward_substitution_m: matrix B must have compatible dimensions");

    qmml::forward_substitution_m(static_cast<double*>(bufX.ptr), static_cast<double*>(bufL.ptr), static_cast<double*>(bufB.ptr), n, m);
}

void _backward_substitution_v(py_cdarray_t x, py_cdarray_t U, py_cdarray_t b)
{
    auto bufx = x.request(), bufU = U.request(), bufb = b.request();

    if(bufx.ndim != 1) throw QMML_ERROR("_backward_substitution_v first argument must be a vector");
    if(bufU.ndim != 2) throw QMML_ERROR("_backward_substitution_v second argument must be a matrix");
    if(bufb.ndim != 1) throw QMML_ERROR("_backward_substitution_v third argument must be a vector");

    const size_t n = bufx.shape[0];

    if(bufU.shape[0] != n || bufU.shape[1] != n) throw QMML_ERROR("_backward_substitution_v: matrix U must have compatible dimensions");
    if(bufb.shape[0] != n) throw QMML_ERROR("_backward_substitution_v: vector b must have compatible dimension");

    qmml::backward_substitution_v(static_cast<double*>(bufx.ptr), static_cast<double*>(bufU.ptr), static_cast<double*>(bufb.ptr), n);
}

void _backward_substitution_m(py_cdarray_t X, py_cdarray_t U, py_cdarray_t B)
{
    auto bufX = X.request(), bufU = U.request(), bufB = B.request();

    if(bufX.ndim != 2) throw QMML_ERROR("_backward_substitution_m first argument must be a matrix");
    if(bufU.ndim != 2) throw QMML_ERROR("_backward_substitution_m second argument must be a matrix");
    if(bufB.ndim != 2) throw QMML_ERROR("_backward_substitution_m third argument must be a matrix");

    const size_t n = bufX.shape[0];
    const size_t m = bufX.shape[1];

    if(bufU.shape[0] != n || bufU.shape[1] != n) throw QMML_ERROR("_backward_substitution_m: matrix U must have compatible dimensions");
    if(bufB.shape[0] != n || bufB.shape[1] != m) throw QMML_ERROR("_backward_substitution_m: matrix B must have compatible dimensions");

    qmml::backward_substitution_m(static_cast<double*>(bufX.ptr), static_cast<double*>(bufU.ptr), static_cast<double*>(bufB.ptr), n, m);
}

void _forward_backward_substitution_v(py_cdarray_t x, py_cdarray_t L, py_cdarray_t b)
{
    auto bufx = x.request(), bufL = L.request(), bufb = b.request();

    if(bufx.ndim != 1) throw QMML_ERROR("_forward_backward_substitution_v first argument must be a vector");
    if(bufL.ndim != 2) throw QMML_ERROR("_forward_backward_substitution_v second argument must be a matrix");
    if(bufb.ndim != 1) throw QMML_ERROR("_forward_backward_substitution_v third argument must be a vector");

    const size_t n = bufx.shape[0];  // number of samples

    if(bufL.shape[0] != n || bufL.shape[1] != n) throw QMML_ERROR("_forward_backward_substitution_v: matrix L must have compatible dimensions");
    if(bufb.shape[0] != n) throw QMML_ERROR("_forward_backward_substitution_v: vector b must have compatible dimension");

    qmml::forward_backward_substitution_v(static_cast<double*>(bufx.ptr), static_cast<double*>(bufL.ptr), static_cast<double*>(bufb.ptr), n);
}

void _backward_forward_substitution_v(py_cdarray_t x, py_cdarray_t U, py_cdarray_t b)
{
    auto bufx = x.request(), bufU = U.request(), bufb = b.request();

    if(bufx.ndim != 1) throw QMML_ERROR("_backward_forward_substitution_v first argument must be a vector");
    if(bufU.ndim != 2) throw QMML_ERROR("_backward_forward_substitution_v second argument must be a matrix");
    if(bufb.ndim != 1) throw QMML_ERROR("_backward_forward_substitution_v third argument must be a vector");

    const size_t n = bufx.shape[0];

    if(bufU.shape[0] != n || bufU.shape[1] != n) throw QMML_ERROR("_backward_forward_substitution_v: matrix U must have compatible dimensions");
    if(bufb.shape[0] != n) throw QMML_ERROR("_backward_forward_substitution_v: vector b must have compatible dimension");

    qmml::backward_forward_substitution_v(static_cast<double*>(bufx.ptr), static_cast<double*>(bufU.ptr), static_cast<double*>(bufb.ptr), n);
}

// Python bindings

void init_numerics(py::module &m)
{
    // lower triangular part
    m.def("_lower_triangular_part_nelems", &_lower_triangular_part_nelems, "Number of components in lower triangular part of n x m matrix", py::arg("n"), py::arg("m"), py::arg("k"));
    m.def("_lower_triangular_part", &_lower_triangular_part, "Lower triangular part of matrix", py::arg("v"), py::arg("M"), py::arg("k"));

    // symmetrize
    m.def("_symmetrize", &_symmetrize, "Symmetrizes quadratic matrix", py::arg("M"));

    // forward and backward substitution
    m.def("_forward_substitution_v", &_forward_substitution_v, "Forward substitution to solve L.x=b for lower triangular matrix L", py::arg("x"), py::arg("L"), py::arg("b"));
    m.def("_forward_substitution_m", &_forward_substitution_m, "Forward substitution to solve L.X=B for lower triangular matrix L", py::arg("X"), py::arg("L"), py::arg("B"));
    m.def("_backward_substitution_v", &_backward_substitution_v, "Backward substitution to solve U.x=b for upper triangular matrix U", py::arg("x"), py::arg("U"), py::arg("b"));
    m.def("_backward_substitution_m", &_backward_substitution_m, "Backward substitution to solve U.X=B for upper triangular matrix U", py::arg("X"), py::arg("U"), py::arg("B"));
    m.def("_forward_backward_substitution_v", &_forward_backward_substitution_v, "Forward and backward substitution to solve L.L^T.x=b for lower triangular matrix L", py::arg("x"), py::arg("L"), py::arg("b"));
    m.def("_backward_forward_substitution_v", &_backward_forward_substitution_v, "Backward and forward substitution to solve U^T.U.x=b for upper triangular matrix L", py::arg("x"), py::arg("U"), py::arg("b"));
}
