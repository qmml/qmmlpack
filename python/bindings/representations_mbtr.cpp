// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Python bindings for many-body tensor representation

#include "bindhelp/representations_mbtr.hpp"
#include "bindings/representations_mbtr.hpp"

#include <initializer_list>

// representations_mbtr

py::array_t<double> _representation_mbtr(
    long k,
    py_clarray_t ssizes, py_clarray_t sinds, py_cdarray_t bases, py_clarray_t zz, py_cdarray_t rr, py_clarray_t elems,
    double xmin, double deltax, long xdim,
    std::string geomf, py::buffer geomfp, std::string weightf, py::buffer weightfp, std::string distrf, py::buffer distrfp,
    std::string corrf, py::buffer corrfp, std::string eindexf, py::buffer eindexfp, std::string aindexf, py::buffer aindexfp,
    double acc, bool flatten
)
{
    auto buf_ssizes = ssizes.request();
    auto buf_sinds  = sinds .request();
    auto buf_bases  = bases .request();
    auto buf_zz     = zz    .request();
    auto buf_rr     = rr    .request();
    auto buf_elems  = elems .request();

    const bool periodic = ( buf_bases.ndim != 0);

    if( buf_ssizes.ndim != 1 || buf_sinds.ndim != 1 || (periodic && buf_bases.ndim != 1) || buf_zz.ndim != 1 || buf_rr.ndim != 1 || buf_elems.ndim != 1)
        throw QMML_ERROR("_representation_mbtr arguments must be linearized.");

    const long ns = buf_ssizes.shape[0];  // number of systems

    if(buf_sinds.shape[0] != ns) throw QMML_ERROR("_representation_mbtr: system sizes and indices must have same size");
    if(periodic && buf_bases.shape[0] != ns*9) throw QMML_ERROR("_representation_mbtr: bases has wrong size");
    if( acc <= 0 ) throw QMML_ERROR("_representation_mbtr: target accuracy must be positive");

    auto buf_geomfp   = geomfp  .request();
    auto buf_weightfp = weightfp.request();
    auto buf_distrfp  = distrfp .request();
    auto buf_corrfp   = corrfp  .request();
    auto buf_eindexfp = eindexfp.request();
    auto buf_aindexfp = aindexfp.request();

    if(buf_geomfp.ndim != 1 || buf_weightfp.ndim != 1 || buf_distrfp.ndim != 1 || buf_corrfp.ndim != 1 || buf_eindexfp.ndim != 1 || buf_aindexfp.ndim != 1)
        throw QMML_ERROR("_representation_mbtr: parametrization parameters have wrong dimensions");

    auto params = bh::mbtr_params<long>(
        k,
        nullptr,
        ns,
        static_cast<long   *>(buf_ssizes.ptr),
        static_cast<long   *>(buf_sinds.ptr ),
        periodic ? static_cast<double *>(buf_bases.ptr) : nullptr,
        static_cast<long   *>(buf_zz.ptr    ),
        static_cast<double *>(buf_rr.ptr    ),
        buf_elems.shape[0],
        static_cast<long   *>(buf_elems.ptr ),
        xmin, deltax, xdim,
        geomf  , buf_geomfp  .shape[0], static_cast<double *>(buf_geomfp  .ptr),
        weightf, buf_weightfp.shape[0], static_cast<double *>(buf_weightfp.ptr),
        distrf , buf_distrfp .shape[0], static_cast<double *>(buf_distrfp .ptr),
        corrf  , buf_corrfp  .shape[0], static_cast<double *>(buf_corrfp  .ptr),
        eindexf, buf_eindexfp.shape[0], static_cast<double *>(buf_eindexfp.ptr),
        aindexf, buf_aindexfp.shape[0], static_cast<double *>(buf_aindexfp.ptr),
        acc
    );

    bh::call_many_body_tensor(params);

    std::vector<size_t> shape, strides;
    if(flatten)
    {
        shape.push_back( params.ns ); shape.push_back( params.mbtr_size / params.ns );
        strides.push_back( shape[1] * sizeof(double) ); strides.push_back( sizeof(double) );
    }
    else
    {
        shape.reserve(params.mbtr_ndim); for(int i = 0; i < params.mbtr_ndim; ++i) shape.push_back(params.mbtr_shape[i]);
        strides.reserve(params.mbtr_ndim); for(int i = 0; i < params.mbtr_ndim; ++i) strides.push_back(params.mbtr_strides[i]);
    }
    py::capsule base(params.mbtr, [](void *p) { delete[] static_cast<double *>(p); } );
    return py::array_t<double>( shape, strides, params.mbtr, base);
}

// Python bindings

void init_representations_mbtr(py::module &m)
{
    m.def("_representation_mbtr", &_representation_mbtr, "Computes many-body tensor representation", py::return_value_policy::take_ownership,
        py::arg("k"), py::arg("ssizes"), py::arg("sinds"), py::arg("bases"), py::arg("zz"), py::arg("rr"),
        py::arg("elems"), py::arg("xmin"), py::arg("deltax"), py::arg("xdim"), py::arg("geomf"), py::arg("geomfp"),
        py::arg("weightf"), py::arg("weightfp"), py::arg("distrf"), py::arg("distrfp"), py::arg("corrf"), py::arg("corrfp"),
        py::arg("eindexf"), py::arg("eindexfp"), py::arg("aindexf"), py::arg("aindexfp"), py::arg("acc"), py::arg("flatten")
    );
}
