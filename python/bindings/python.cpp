// qmmlpack
// (c) Matthias Rupp, 2006-2017.
// See LICENSE.txt for license.

// Python bindings

#include "python.hpp"

#include "distances_pnorm.hpp"
#include "io_extxyz.hpp"
#include "kernels_centering.hpp"
#include "kernels_linear.hpp"
#include "kernels_gaussian.hpp"
#include "kernels_laplacian.hpp"
#include "numerics.hpp"
#include "representations_mbtr.hpp"

// Some preprocessor magic to get the library name right (once as a symbol, once as a string).
// Simply concatenating/stringifying directly does not work because # and ## inhibit macro expansion
#define PRIMITIVE_CONCATENATE(arg1,arg2) arg1 ## arg2
#define CONCATENATE(arg1,arg2) PRIMITIVE_CONCATENATE(arg1,arg2)
#define LIBRARYNAME CONCATENATE(libqmmlpackpy,VERSION_MAJOR)
// Two levels of macros are required when stringifying the result of expansions for GCC
#define PRIMITIVE_STRINGIFY(arg) #arg
#define STRINGIFY(arg) PRIMITIVE_STRINGIFY(arg)
#define LIBRARYNAMESTR STRINGIFY(LIBRARYNAME)

// Python library calls _register_exception(QMMLException) during initialization
static py::object _qmml_exception_class;
void _register_exception(py::object exc_class)
{
    _qmml_exception_class = exc_class;
}

PYBIND11_PLUGIN(LIBRARYNAME)
{
    py::module m(LIBRARYNAMESTR, "qmmlpack C++ bindings.");

    // exception handling. all exceptions are translated to qmmlpack.QMMLException
    m.def("_register_exception", &_register_exception, "Registers qmmlpack.QMMLException class", py::arg("exception class"));

    py::register_exception_translator( [](std::exception_ptr p)
    {
       try { if(p) std::rethrow_exception(p); }
       catch(qmml::error const& e)
       {
           PyErr_SetString(_qmml_exception_class.ptr(), e.what());
       }
    });

    // initialization of library parts
    init_distances_pnorm(m);
    init_io_extxyz(m);
    init_kernels_centering(m);
    init_kernels_linear(m);
    init_kernels_gaussian(m);
    init_kernels_laplacian(m);
    init_numerics(m);
    init_representations_mbtr(m);

    return m.ptr();
}
