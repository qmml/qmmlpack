# qmmlpack
# (c) Matthias Rupp, 2006-2017.
# See LICENSE.txt for license.

"""Python unit tests for Laplacian kernel.

Part of qmmlpack library.
"""

import pytest
import numpy as np
from math import exp, sqrt

import qmmlpack as qmml

def _equal(a, b):
    # Equality test for floating point arrays
    return np.allclose(a, b)

#  ############################
#  #  From distance matrix D  #
#  ############################

class TestKernelLaplacianD:
    def test_one_sample(self):
        dd = np.array( [ [ 0 ] ] );
        rr = np.array( [ [ 1 ] ] );
        assert _equal(qmml.kernel_laplacian(dd, theta=[1], distance=True), rr)

    def test_two_samples1(self):
        dd = np.array( [ [ 0, 17 ], [ 17, 0 ] ] );
        rr = np.array( [ [ 1, exp(-17./2) ], [ exp(-17./2), 1 ] ] );
        assert _equal(qmml.kernel_laplacian(dd, theta=[2], distance=True), rr)

    def test_two_samples2(self):
        # check also passing a scalar as parameter
        dd = np.array( [ [ 0, 8 ], [ 8, 0 ] ] );
        rr = np.array( [ [ 1, exp(-2) ], [ exp(-2), 1 ] ] );
        assert _equal(qmml.kernel_laplacian(dd, theta=4, distance=True), rr)

    def test_distance_diagonal(self):
        dd = np.array( [ [ 0, 17 ], [ 17, 0 ] ] );
        rr = np.array( [ 1, 1 ] );
        assert _equal(qmml.kernel_laplacian(dd, theta=[1], distance=True, diagonal=True), rr)

    def test_symmetry(self):
        kc = qmml.kernel_laplacian(qmml.distance_one_norm(np.random.uniform(-1, 1, (   2,   1))), theta=  1, distance=True); assert ( kc == kc.T ).all()
        kc = qmml.kernel_laplacian(qmml.distance_one_norm(np.random.uniform(-1, 1, (  20,  10))), theta=  6, distance=True); assert ( kc == kc.T ).all()
        kc = qmml.kernel_laplacian(qmml.distance_one_norm(np.random.uniform(-1, 1, ( 100, 500))), theta=331, distance=True); assert ( kc == kc.T ).all()
        kc = qmml.kernel_laplacian(qmml.distance_one_norm(np.random.uniform(-1, 1, (1000, 100))), theta= 67, distance=True); assert ( kc == kc.T ).all()


#  ############################
#  #  From distance matrix E  #
#  ############################

class TestKernelLaplacianE:
    def test_one_vs_one(self):
        ee = np.array( [ [ 1 ] ] );
        rr = np.array( [ [ exp(-1) ] ] );
        assert _equal(qmml.kernel_laplacian(ee, theta=[1], distance=True), rr)

    def three_vs_two(self):
        ee = np.array( [ [ 2, 9 ], [ 7, 4 ], [ 3, 8 ] ] )
        rr = np.array( [ [ exp(-4), exp(-18) ], [ exp(-14), exp(-8) ], [ exp(-6), exp(-16) ] ] );
        assert _equal(qmml.kernel_laplacian(ee, theta=[0.5], distance=True), rr)

    def two_vs_three(self):
        # check also passing a scalar as parameter
        ee = np.array( [ [ 4, 2, 2 ], [ 3, 3, 1 ] ] );
        rr = np.array( [ [ exp(-8), exp(-4), exp(-4) ], [ exp(-6), exp(-6), exp(-2) ] ] );
        assert _equal(qmml.kernel_laplacian(ee, theta=0.5, distance=True), rr)


#  #####################
#  #  Kernel matrix K  #
#  #####################

class TestKernelLaplacianK:
    def test_one_sample(self):
        xx = np.array( [ [ 1 ] ] );
        rr = np.array( [ [ 1 ] ] );
        assert _equal(qmml.kernel_laplacian(xx, theta=[1.]), rr)

    def test_two_samples1(self):
        xx = np.array( [ [ 1, -1 ], [ 2, 3 ] ] );
        rr = np.array( [ [ 1, exp(-5) ], [ exp(-5), 1 ] ] );
        assert _equal(qmml.kernel_laplacian(xx, theta=[1.]), rr)

    def test_two_samples2(self):
        # check also passing a scalar as parameter
        xx = np.array( [ [ 1, 2 ], [ 3, 4 ] ] );
        rr = np.array( [ [ 1, exp(-2) ], [ exp(-2), 1 ] ] );
        assert _equal(qmml.kernel_laplacian(xx, theta=2), rr)

    def test_symmetry(self):
        kc = qmml.kernel_laplacian(np.random.uniform(-1, 1, (  2,    1)), theta=  1); assert ( kc == kc.T ).all()
        kc = qmml.kernel_laplacian(np.random.uniform(-1, 1, (  20,  10)), theta=  6); assert ( kc == kc.T ).all()
        kc = qmml.kernel_laplacian(np.random.uniform(-1, 1, ( 100, 500)), theta=331); assert ( kc == kc.T ).all()
        kc = qmml.kernel_laplacian(np.random.uniform(-1, 1, (1000, 100)), theta= 67); assert ( kc == kc.T ).all()


#  #####################
#  #  Kernel matrix L  #
#  #####################

class TestKernelLaplacianL:
    def test_one_vs_one1(self):
        xx = np.array( [ [ 1 ] ] );
        zz = np.array( [ [ 2 ] ] );
        rr = np.array( [ [ exp(-1) ] ] );
        assert _equal(qmml.kernel_laplacian(xx, zz, theta=[1]), rr)

    def test_one_vs_one2(self):
        xx = np.array( [ [ 1, -1 ] ] );
        zz = np.array( [ [ 2,  3 ] ] );
        rr = np.array( [ [ exp(-5) ] ] );
        assert _equal(qmml.kernel_laplacian(xx, zz, theta=[1]), rr)

    def test_three_vs_two1(self):
        xx = np.array( [ [ 1, -1 ], [ 2, 3 ], [ -1, 2 ] ] );
        zz = np.array( [ [ -1, -1 ], [ 4, 5 ] ] );
        rr = np.array( [ [ exp(-2), exp(-9) ], [ exp(-7), exp(-4) ], [ exp(-3), exp(-8) ] ] );
        assert _equal(qmml.kernel_laplacian(xx, zz, theta=[1]), rr)

    def test_three_vs_two2(self):
        # check also passing a scalar as parameter
        xx = np.array( [ [ -2, 1 ], [ 0, 1 ], [ -1, 2 ] ] );
        zz = np.array( [ [ 1, 2 ], [ -1, 3 ] ] );
        rr = np.array( [ [ exp(-8), exp(-6) ], [ exp(-4), exp(-6) ], [ exp(-4), exp(-2) ] ] );
        assert _equal(qmml.kernel_laplacian(xx, zz, theta=0.5), rr)

    def test_two_vs_three(self):
        xx = np.array( [ [ 1, 2 ], [ -1, 3 ] ] );
        zz = np.array( [ [ -2, 1 ], [ 0, 1 ], [ -1, 2 ] ] );
        rr = np.array( [ [ exp(-8), exp(-4), exp(-4) ], [ exp(-6), exp(-6), exp(-2) ] ] );
        assert _equal(qmml.kernel_laplacian(xx, zz, theta=[0.5]), rr)


#  #####################
#  #  Kernel vector m  #
#  #####################

class TestKernelLaplacianM:
    def test_one_sample_length_1(self):
        xx = np.array( [ [ 1 ] ] );
        rr = np.array( [ [ 1 ] ] );
        assert _equal(qmml.kernel_laplacian(xx, theta=[10], diagonal=True), rr)

    def test_three_samples(self):
        # check also passing a scalar as parameter
        xx = np.array( [ [ 1, 2 ], [ 3, 4 ], [ 5, 6 ] ] );
        rr = np.array( [ [ 1, 1, 1 ] ] );
        assert _equal(qmml.kernel_laplacian(xx, theta=1, diagonal=True), rr)
