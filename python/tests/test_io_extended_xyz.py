# qmmlpack
# (c) Matthias Rupp, 2006-2019.
# See LICENSE.txt for license.

"""Unit tests for importing data in extended XYZ format.

Part of qmmlpack library.
"""

# 2016-04-15 MR  Added tests for non-EOL-terminated input (file and string)

import pytest
import tempfile, os, sys, string, math, random as rnd
import numpy as np

import qmmlpack as qmml

#  ##########################################################
#  #  Original Python implementation, now used for testing  #
#  ##########################################################

from collections import namedtuple

_ExtXYZData = namedtuple("_ExtXYZData", "an xyz mp ap addp")

_ExtXYZData.__doc__ = """Container for data of a molecule in extended XYZ format.

Interface are 5 fields:
an -- atomic numbers
xyz -- coordinates (original units, usually angstrom)
mp -- molecular properties
ap -- atomic properties
addp -- additional properties

Coordinates are lists of lists (as opposed to NumPy arrays) to avoid dependency on NumPy.
This data structure is not a Molecule class of any sort.
"""

#  Elementary parsers

#  Accept indexable source s, current index i.
#  Return new index or (new index, parsed entity) pair.

def _skip_ws_eol(s, i, n):
    """Skips zero or more whitespace characters, including newline."""
    while i < n and s[i] in (0x20, 0x09, 0x0b, 0x0c, 0x0d, 0x0a):
        i += 1
    return i

def _skip_ws(s, i):
    """Skips zero or more whitespace characters ( \\t\\v\\f\\r) except newline (\\n)."""
    while s[i] in (0x20, 0x09, 0x0b, 0x0c, 0x0d):
        i += 1
    return i

def _skip_eol(s, i):
    """Skips zero or more arbitrary characters until and including first newline."""
    while s[i] != 0x0a:
        i += 1
    return i+1

def _parse_token(s, i, interpret = False):
    """Parses a contiguous sequence of non-whitespace characters."""
    j = i; pint = interpret; pfloat = interpret;
    if s[i] in (0x2d, 0x2b): i += 1 # +, -
    while s[i] not in (0x20, 0x09, 0x0b, 0x0c, 0x0d, 0x0a):
        if pint and (s[i] < 0x30 or s[i] > 0x39): pint = False
        if pfloat and s[i] not in (0x2d, 0x2b, 0x30, 0x31, 0x32, 0x33, 0x34, \
                                   0x35, 0x36, 0x37, 0x38, 0x39, 0x2e, 0x45, \
                                   0x65): pfloat = False
        i += 1

    # if token could be an integer, try to parse it
    if pint:
        try: return _parse_int(s, j)
        except ValueError: pass

    # if token could be a float, try to parse it
    if pfloat:
        try: return _parse_float(s, j)
        except ValueError: pass

    return (i, str(s[j:i], 'ascii'))

def _parse_int(s, i):
    """Parses an integer value."""
    j = i
    if s[i] in (0x2d, 0x2b): # +, -
        i += 1
    while s[i] >= 0x30 and s[i] <= 0x39:
        i += 1
    return (i, int(s[j:i]))

def _parse_float(s, i):
    """Parses a floating point value."""
    j = i
    # Optional opening sign
    if s[i] in (0x2d, 0x2b): i += 1
    # Optional part before comma
    while s[i] >= 0x30 and s[i] <= 0x39: i += 1
    # Optional comma
    if s[i] == 0x2e: i += 1
    # Optional part after comma
    while s[i] >= 0x30 and s[i] <= 0x39: i += 1
    # Optional exponential notation
    if s[i] in (0x45, 0x65):
        i, skip = _parse_int(s, i+1)
    return (i, float(str(s[j:i], 'ascii')))

_element_data_an = {
     'H':  1, 'He':  2, 'Li':  3, 'Be':  4,  'B':  5,  'C':  6,  'N':  7,
     'O':  8,  'F':  9, 'Ne': 10, 'Na': 11, 'Mg': 12, 'Al': 13, 'Si': 14,
     'P': 15,  'S': 16, 'Cl': 17, 'Ar': 18,  'K': 19, 'Ca': 20, 'Sc': 21,
    'Ti': 22,  'V': 23, 'Cr': 24, 'Mn': 25, 'Fe': 26, 'Co': 27, 'Ni': 28,
    'Cu': 29, 'Zn': 30, 'Ga': 31, 'Ge': 32, 'As': 33, 'Se': 34, 'Br': 35,
    'Kr': 36, 'Rb': 37, 'Sr': 38,  'Y': 39, 'Zr': 40, 'Nb': 41, 'Mo': 42,
    'Tc': 43, 'Ru': 44, 'Rh': 45, 'Pd': 46, 'Ag': 47, 'Cd': 48, 'In': 49,
    'Sn': 50, 'Sb': 51, 'Te': 52,  'I': 53, 'Xe': 54, 'Cs': 55, 'Ba': 56,
    'La': 57, 'Ce': 58, 'Pr': 59, 'Nd': 60, 'Pm': 61, 'Sm': 62, 'Eu': 63,
    'Gd': 64, 'Tb': 65, 'Dy': 66, 'Ho': 67, 'Er': 68, 'Tm': 69, 'Yb': 70,
    'Lu': 71, 'Hf': 72, 'Ta': 73,  'W': 74, 'Re': 75, 'Os': 76, 'Ir': 77,
    'Pt': 78, 'Au': 79, 'Hg': 80, 'Tl': 81, 'Pb': 82, 'Bi': 83, 'Po': 84,
    'At': 85, 'Rn': 86, 'Fr': 87, 'Ra': 88, 'Ac': 89, 'Th': 90, 'Pa': 91,
     'U': 92, 'Np': 93, 'Pu': 94, 'Am': 95, 'Cm': 96, 'Bk': 97, 'Cf': 98,
    'Es': 99, 'Fm':100, 'Md':101, 'No':102, 'Lr':103, 'Rf':104, 'Db':105,
    'Sg':106, 'Bh':107, 'Hs':108, 'Mt':109, 'Ds':110, 'Rg':111, 'Cn':112,
    'Uut': 113, 'Fl': 114, 'Uup': 115, 'Lv': 116, 'Uus': 117, 'Uuo': 118,

      '1':  1,  '2':  2,  '3':  3,  '4':  4,  '5':  5,  '6':  6,  '7':  7,
      '8':  8,  '9':  9, '10': 10, '11': 11, '12': 12, '13': 13, '14': 14,
     '15': 15, '16': 16, '17': 17, '18': 18, '19': 19, '20': 20, '21': 21,
     '22': 22, '23': 23, '24': 24, '25': 25, '26': 26, '27': 27, '28': 28,
     '29': 29, '30': 30, '31': 31, '32': 32, '33': 33, '34': 34, '35': 35,
     '36': 36, '37': 37, '38': 38, '39': 39, '40': 40, '41': 41, '42': 42,
     '43': 43, '44': 44, '45': 45, '46': 46, '47': 47, '48': 48, '49': 49,
     '50': 50, '51': 51, '52': 52, '53': 53, '54': 54, '55': 55, '56': 56,
     '57': 57, '58': 58, '59': 59, '60': 60, '61': 61, '62': 62, '63': 63,
     '64': 64, '65': 65, '66': 66, '67': 67, '68': 68, '69': 69, '70': 70,
     '71': 71, '72': 72, '73': 73, '74': 74, '75': 75, '76': 76, '77': 77,
     '78': 78, '79': 79, '80': 80, '81': 81, '82': 82, '83': 83, '84': 84,
     '85': 85, '86': 86, '87': 87, '88': 88, '89': 89, '90': 90, '91': 91,
     '92': 92, '93': 93, '94': 94, '95': 95, '96': 96, '97': 97, '98': 98,
     '99': 99,'100':100,'101':101,'102':102,'103':103,'104':104,'105':105,
    '106':106,'107':107,'108':108,'109':109,'110':110,'111':111,'112':112,
    '113':113,'114':114,'115':115,'116':116,'117':117,'118':118,'119':119
}

def _parse_element(s, i):
    """Parses element abbreviation or atomic number."""
    i, token = _parse_token(s, i, False)
    return (i, _element_data_an[token])

def _parse_exyz_entry(s, i, n, addprops):
    """Parses one molecule in extended XYZ format."""

    # If at end of file, return None
    if i >= n: return i, None

    # Skip detritus empty lines at end-of-file or in-between
    i = _skip_ws_eol(s, i, n)
    if i >= n: return i, None

    # Header line: ws* int any* \n
    i = _skip_ws(s, i)
    i, k = _parse_int(s, i)
    i = _skip_eol(s, i)

    an, xyz, mp, ap, addp = [0]*k, [[0,0,0]]*k, [], [[] for j in range(k)], None

    # Molecular properties line (ws* token)* ws* \n
    i = _skip_ws(s, i)
    while s[i] != 0x0a:
        i, token = _parse_token(s, i, True)
        mp.append(token)
        i = _skip_ws(s, i)
    i += 1 # eol

    # Atom block lines ws* el ws+ x ws+ y ws+ z ws+ (ws* token)* \n
    for j in range(k):
        # atomic number
        i = _skip_ws(s, i)
        i, an[j] = _parse_element(s, i)

        # coordinates
        i = _skip_ws(s, i)
        i, x = _parse_float(s, i)
        i = _skip_ws(s, i)
        i, y = _parse_float(s, i)
        i = _skip_ws(s, i)
        i, z = _parse_float(s, i)
        xyz[j] = [x, y, z]

        # atomic properties
        i = _skip_ws(s, i)
        while s[i] != 0x0a:
            i, token = _parse_token(s, i, True)
            ap[j].append(token)
            i = _skip_ws(s, i)
        i += 1 # eol

    # Additional properties
    if addprops:
        addp = []
        i = _skip_ws(s, i)
        while s[i] != 0x0a:
            line = []
            while s[i] != 0x0a:
                i, token = _parse_token(s, i, True)
                line.append(token)
                i = _skip_ws(s, i)
            i += 1 # eol
            addp.append(line)
            i = _skip_ws(s, i)
        i += 1 # eol

    return i, _ExtXYZData(an, xyz, mp, ap, addp)

#  Generator and function interface

import mmap # memory-mapped files

class _index_wrapper:
    """Wraps index access to fake trailing end-of-line character."""

    def __init__(self, s, n):
        """Stores original indexable object and size."""
        (self.s, self.n) = (s, n)

    def __getitem__(self, i):
        """Original indexing, except last byte is end-of-line."""
        try:
            return self.s[i]
        except IndexError:
            if i == self.n: return 0x0a
            raise

    def __getattr__(self, name):
        """Passes through attribute lookup."""
        return self.s.__getattribute__(name)

    def close(self):
        """Passes through calls to close()."""
        return self.s.close()

def _is_filename(s):
    """True if s is a filename (as opposed to first line of XYZ format)."""
    i = 0
    while i < len(s) and s[i] != "\n":
        if s[i] not in "0123456789 \t\v\f\r":
            return True
        i += 1
    return i >= len(s)

class _ExtendedXYZReader:
    """Import iterator-style from extended XYZ file format.

    Supports generator and iterator usage:
    >>> with ExtendedXYZReader(source) as reader: [mol for mol in reader]
    """

    def __init__(self, source, additional_properties = False):
        """Creates reader from file or string.

        If source is a filename, input is read from that file.
        If source is a file, input is read from that file. File is not closed.
        If source is a string in extended XYZ format, input is read directly from the string.

        If additional_properties is True, parses additional property lines after each molecule.
        In this case, molecule blocks have to be terminated by an empty line.
        """
        self.addprops = additional_properties

        # Setup according to type of source
        if type(source) == bytes:
            self.file, self.s, self.n = None, source, len(source)
        elif isinstance(source, str):
            # If source is a file, open it, as a memory-mapped file for performance
            if _is_filename(source):
                self.file = open(source, "rb")
                self.s = mmap.mmap(self.file.fileno(), 0, access=mmap.ACCESS_READ)
                self.n = self.s.size() # equals file size due to map size chosen by 0
            else:
                self.file = None
                self.s = source.encode("utf-8")
                self.n = len(self.s)
        elif 'read' in dir(source):
            # Assumes that source is a file
            # Whole file is read into memory. If undesired, pass a buffering object.
            self.file = None
            self.s = source.read()
            self.n = len(self.s)
        else:
            # Unknown type, assume it has indexing and length defined
            self.file, self.s, self.n = None, source, len(source)

        # File must be whitespace-terminated. If not, use wrapper.
        if self.s[self.n-1] not in (0x20, 0x09, 0x0b, 0x0c, 0x0d, 0x0a):
            self.s = _index_wrapper(self.s, self.n)

    def __enter__(self):
        # with statement context generator usage, see 3.3.8 in python reference
        self.ind = 0
        return self

    def __exit__(self, *exception):
        # with statement context generator usage, see 3.3.8 in python reference
        self.close()
        if exception != (None, None, None): return False
        return True

    def close(self):
        """Releases any resources acquired."""
        if self.file != None:
            if self.s != None:
                if not self.s.closed: self.s.close()
                self.s = None
            self.file.close()
            self.file = None
        else:
            self.s = None

    def __iter__(self):
        return self

    def __next__(self):
        self.ind, mol = _parse_exyz_entry(self.s, self.ind, self.n, self.addprops)
        if mol != None:
            return mol
        else:
            raise StopIteration

def _read_exyz(*args, **kwargs):
    """Imports all atomistic systems from extended XYZ format.

    Accepts the same arguments as ExtendedXYZReader.
    """
    with _ExtendedXYZReader(*args, **kwargs) as reader:
        return [mol for mol in reader]

_element_data_abbrv = {
      1: 'H ',  2: 'He',  3: 'Li',  4: 'Be',  5: 'B ',  6: 'C ',  7: 'N ',
      8: 'O ',  9: 'F ', 10: 'Ne', 11: 'Na', 12: 'Mg', 13: 'Al', 14: 'Si',
     15: 'P ', 16: 'S ', 17: 'Cl', 18: 'Ar', 19: 'K ', 20: 'Ca', 21: 'Sc',
     22: 'Ti', 23: 'V ', 24: 'Cr', 25: 'Mn', 26: 'Fe', 27: 'Co', 28: 'Ni',
     29: 'Cu', 30: 'Zn', 31: 'Ga', 32: 'Ge', 33: 'As', 34: 'Se', 35: 'Br',
     36: 'Kr', 37: 'Rb', 38: 'Sr', 39: 'Y ', 40: 'Zr', 41: 'Nb', 42: 'Mo',
     43: 'Tc', 44: 'Ru', 45: 'Rh', 46: 'Pd', 47: 'Ag', 48: 'Cd', 49: 'In',
     50: 'Sn', 51: 'Sb', 52: 'Te', 53: 'I ', 54: 'Xe', 55: 'Cs', 56: 'Ba',
     57: 'La', 58: 'Ce', 59: 'Pr', 60: 'Nd', 61: 'Pm', 62: 'Sm', 63: 'Eu',
     64: 'Gd', 65: 'Tb', 66: 'Dy', 67: 'Ho', 68: 'Er', 69: 'Tm', 70: 'Yb',
     71: 'Lu', 72: 'Hf', 73: 'Ta', 74: 'W ', 75: 'Re', 76: 'Os', 77: 'Ir',
     78: 'Pt', 79: 'Au', 80: 'Hg', 81: 'Tl', 82: 'Pb', 83: 'Bi', 84: 'Po',
     85: 'At', 86: 'Rn', 87: 'Fr', 88: 'Ra', 89: 'Ac', 90: 'Th', 91: 'Pa',
     92: 'U ', 93: 'Np', 94: 'Pu', 95: 'Am', 96: 'Cm', 97: 'Bk', 98: 'Cf',
     99: 'Es',100: 'Fm',101: 'Md',102: 'No',103: 'Lr',104: 'Rf',105: 'Db',
    106: 'Sg',107: 'Bh',108: 'Hs',109: 'Mt',110: 'Ds',111: 'Rg',112: 'Cn',
    113:'Uut',114: 'Fl',115:'Uup',116: 'Lv',117:'Uus',118:'Uuo'
}

def _format_exyz_entry(mol):
    """Formats molecule in extended XYZ format.

    Example:
    >>> _format_exyz_entry(_ExtXYZData([7, 7], [[0,0,0],[0,0,1.45]], [N2 40.6], None)
    """
    n = len(mol.an)
    def fmt(tok):
        return {str: '{}', int: '{:d}', float: "{: f}"}[type(tok)].format(tok)

    molpropline = ' '.join([fmt(tok) for tok in mol.mp])

    atomblock = ['{} {: f} {: f} {: f}'.format(_element_data_abbrv[mol.an[i]],
        mol.xyz[i][0], mol.xyz[i][1], mol.xyz[i][2]) for i in range(n)]
    if mol.ap != None:
        for i in range(n):
            if mol.ap[i] != []:
                atomblock[i] += '  ' + ' '.join([fmt(tok) for tok in mol.ap[i]])

    addprops = ''
    if mol.addp != None and mol.addp != []:
        addprops += '\n'.join([' '.join([fmt(tok) for tok in line]) for line in mol.addp]) + '\n\n'

    return str(n) + '\n' + molpropline + '\n' + '\n'.join(atomblock) + '\n' + addprops

class _ExtendedXYZWriter:
    """Export iterator-style to extended XYZ file format."""

    def __init__(self, sink):
        """Creates writer to file or list.

        If sink is a filename, molecules are written to that file in extended XYZ format.
        If sink is a list, molecules are appended to that list as strings in extended XYZ format.
        """
        # Setup according to type of sink
        self.file, self.list = None, None
        if type(sink) == str:
            self.file = open(sink, "wb")
        elif type(sink) == list:
            self.list = sink

    def __enter__(self):
        # with statement context generator usage, see 3.3.8 in python reference
        return self

    def __exit__(self, *exception):
        # with statement context generator usage, see 3.3.8 in python reference
        self.close()
        if exception != (None, None, None): raise
        return True

    def close(self):
        """Releases any resources acquired."""
        if self.file != None:
            self.file.close()
            self.file = None
        if self.list != None:
            self.list = None

    def write(self, mol):
        """Writes molecule as string in extended XYZ format (see _ExtXYZData)."""
        m = _format_exyz_entry(mol)
        if self.file != None:
            self.file.write(m.encode('ascii'))
        if self.list != None:
            self.list.append(m)

def _write_exyz(sink, mols):
    """Exports all atomistic systems from extended XYZ format.

    sink -- where to write to. Can be None, returning a list of formatted strings, a filename, or a list.
    mols -- list of molecules to write to sink. See _ExtXYZData.
    """
    if sink is None:
        return [_format_exyz_entry(mol) for mol in mols]
    else:
        with ExtendedXYZWriter(sink) as writer:
            for mol in mols:
                writer.write(mol)

#  ###########################################
#  #  End of original Python implementation  #
#  ###########################################

#  #########################
#  #  Begin of unit tests  #
#  #########################

def _rounded_equal(lhs, rhs):
    """Approximate equality test for numeric arguments."""
    if type(lhs) == float:
        return math.fabs(lhs - rhs) < 10e-6
    elif type(lhs) == list:
        if len(lhs) != len(rhs): return False
        return all(_rounded_equal(a,b) for (a,b) in zip(lhs, rhs))
    elif isinstance(lhs, np.ndarray):
        return np.allclose(lhs, rhs, rtol = 0., atol = 10e-6)
    else:
        return lhs == rhs

def _equal(lhs, rhs):
    """Equality test of extended XYZ format data for testing."""
    return (lhs.an == rhs.an).all() \
        and _rounded_equal(lhs.xyz , rhs.xyz ) \
        and _rounded_equal(lhs.mp  , rhs.mp  ) \
        and _rounded_equal(lhs.ap  , rhs.ap  ) \
        and _rounded_equal(lhs.addp, rhs.addp)

class TestReadElementTypes:
    # All tests related to element specifications

    def test_abbreviations(self):
        # standard one to three letter abbreviations
        els = b"H He Li Be B C N O F Ne Na Mg Al Si P S Cl Ar K Ca Sc Ti V " \
              b"Cr Mn Fe Co Ni Cu Zn Ga Ge As Se Br Kr Rb Sr Y Zr Nb Mo Tc " \
              b"Ru Rh Pd Ag Cd In Sn Sb Te I Xe Cs Ba La Ce Pr Nd Pm Sm Eu " \
              b"Gd Tb Dy Ho Er Tm Yb Lu Hf Ta W Re Os Ir Pt Au Hg Tl Pb Bi " \
              b"Po At Rn Fr Ra Ac Th Pa U Np Pu Am Cm Bk Cf Es Fm Md No Lr " \
              b"Rf Db Sg Bh Hs Mt Ds Rg Cn Uut Fl Uup Lv Uus Uuo"
        s = b"118\n\n" + b"".join([el + b" 1 2 3\n" for el in els.split()])
        res = qmml.import_extxyz(s)
        assert ( res[0].an == list(range(1, 119)) ).all()

    def test_atomic_numbers(self):
        # atomic number as element specifications
        els = b"  1   2   3   4   5   6   7   8   9  10  11  12  13  14  15 " \
              b" 16  17  18  19  20  21  22  23  24  25  26  27  28  29  30 " \
              b" 31  32  33  34  35  36  37  38  39  40  41  42  43  44  45 " \
              b" 46  47  48  49  50  51  52  53  54  55  56  57  58  59  60 " \
              b" 61  62  63  64  65  66  67  68  69  70  71  72  73  74  75 " \
              b" 76  77  78  79  80  81  82  83  84  85  86  87  88  89  90 " \
              b" 91  92  93  94  95  96  97  98  99 100 101 102 103 104 105 " \
              b"106 107 108 109 110 111 112 113 114 115 116 117 118"
        s = b"118\n\n" + b"".join([el + b" 1 2 3\n" for el in els.split()])
        res = qmml.import_extxyz(s)
        assert ( res[0].an == list(range(1, 119)) ).all()

class TestReadCoordinates:
    # All tests related to coordinate specifications

    def test_float_formats(self):
        # test different formats for floating point numbers
        s = r"""4

        C  1       2.0     3.000001
        C -1      -2.0    -3.001
        C  1.2e-1 +1.2e-1 -1.2E-1
        C  0.       .0     0
        """
        res = qmml.import_extxyz(s)
        assert _rounded_equal(res[0].xyz , np.asarray([
            [ 1.0 ,  2.0 ,  3.000001],
            [-1.0 , -2.0 , -3.001   ],
            [ 0.12,  0.12, -0.12    ],
            [ 0.  ,  0.  ,  0.      ]
        ]))

class TestReadMolecularProperties:
    # All tests related to molecular properties

    def test_string_properties(self):
        # pure string molecular properties
        s = "1\n ABC\tdef\t\t string_token\nC 1 2 3\n";
        res = qmml.import_extxyz(s)
        assert res[0].mp == ["ABC", "def", "string_token"]

    def test_integer_properties(self):
        # integer molecular properties
        s = "1\n abc 123 ABC\nC 1 2 3\n";
        res = qmml.import_extxyz(s)
        assert res[0].mp == ["abc", 123, "ABC"]

    def test_float_properties(self):
        # floating point molecular properties
        s = "1\n abc ABC 1.23 -0.1E+1\nC 1 2 3\n";
        res = qmml.import_extxyz(s)
        assert _rounded_equal(res[0].mp, ["abc", "ABC", 1.23, -1.0])

class TestReadAtomicProperties:
    # All tests related to atomic properties

    def test_string_properties(self):
        # pure string atomic properties
        s = "2\n\nC 1 2 3 abc ABC\nC 1 2 3 def\n"
        res = qmml.import_extxyz(s)
        assert res[0].ap == [["abc", "ABC"], ["def"]]

    def test_integer_properties(self):
        # integer atomic properties
        s = "2\n\nC 1 2 3 abc 123 ABC\nB 1 2 3 456\n"
        res = qmml.import_extxyz(s)
        assert res[0].ap == [["abc", 123, "ABC"], [456]]

    def test_float_properties(self):
        # floating point atomic properties
        s = "2\n\nC 1 2 3 abc 1.23 ABC\nB 1 2 3 4.56 4e-1\n"
        res = qmml.import_extxyz(s)
        assert _rounded_equal(res[0].ap, [["abc", 1.23, "ABC"], [4.56, 0.4]])

class TestReadAdditionalProperties:
    # All tests related to additional properties

    def test_single_line(self):
        # one additional property line
        s = "1\n\nC 1 2 3\nabc 123\n \t\n"
        res = qmml.import_extxyz(s, additional_properties = True)
        assert res[0].addp == [["abc", 123]]

    def test_two_lines(self):
        # two additional property lines
        s = "1\n\nC 1 2 3\nabc   \n \t\t 4.56\t \n\n"
        res = qmml.import_extxyz(s, additional_properties = True)
        assert _rounded_equal(res[0].addp, [["abc"], [4.56]])

    def test_three_molecules(self):
        # three molecules with additional properties
        s = "1\n\nC 1 2 3\nabc\n\n1\n\nC 1 2 3\n123 4.56\n\n1\n\nC 1 2 3\n\n"
        res = qmml.import_extxyz(s, additional_properties = True)
        assert _rounded_equal([m.addp for m in res], [[["abc"]], [[123, 4.56]], []])

class TestReadSpecialCases:
    """Various special cases."""

    def test_floating_point_cases(self):
        s = "1\n0 0. 0.0 .0\nC 1 2 3\n"
        res = qmml.import_extxyz(s)
        assert _rounded_equal(res[0].mp, [0, 0., 0., 0.])

    def test_e_notation(self):
        s = "1\n1.23E-1\nC 1 2 3\n"
        res = qmml.import_extxyz(s)
        assert _rounded_equal(res[0].mp, [0.123])

    # 2016-04-14 Test whether parsing works if last line is not terminated by EOL
    def test_reading_trailing_eol(self):
        s1 = "2\n\nC 1 2 3 abc 1.23 ABC\nB 1 2 3 4.56 4e-1\n" # terminated
        s2 = "2\n\nC 1 2 3 abc 1.23 ABC\nB 1 2 3 4.56 4e-1"   # not terminated
        res1 = qmml.import_extxyz(s1)
        res2 = qmml.import_extxyz(s2)
        assert _equal(res1[0], res2[0])

    def test_two_molecules(self):
        # Parsing of two molecules, no separating empty line
        s = "2\n abc\nC 1 2 3 X\nB 4 5 6 4321\n1\n\nH 7 8 99 \n"
        res = qmml.import_extxyz(s)
        assert _equal(res[0], qmml.ExtXYZData([6,5], [[1.,2.,3.],[4.,5.,6.]], ["abc"], [["X"],[4321]], []))
        assert _equal(res[1], qmml.ExtXYZData([1], [[7.,8.,99.]], [], [[]], []))

class TestReadFiles:
    # Test whether reading from and writing to files works

    def test_reading_from_file1(self):
        # Reads input from file; input is EOL-terminated
        (fd,fn) = tempfile.mkstemp(suffix = ".xyz")
        with open(fd, 'wt', newline = '\n') as f:
            f.write("1\n mp 123\n C 1 2 3 abc 9.1\n")

        res = qmml.import_extxyz(fn)
        os.remove(fn)
        assert _equal(res[0], qmml.ExtXYZData([6], [[1.,2.,3.]], ["mp",123], [["abc", 9.1]], []))

    def test_reading_from_file2(self):
        # Reads input from file; input is not EOL-terminated
        (fd,fn) = tempfile.mkstemp(suffix = ".xyz")
        with open(fd, 'wt', newline = '\n') as f:
            f.write("1\n mp 123\n C 1 2 3 abc 9.1")

        res = qmml.import_extxyz(fn)
        os.remove(fn)
        assert _equal(res[0], qmml.ExtXYZData([6], [[1.,2.,3.]], ["mp",123], [["abc", 9.1]], []))

# TODO: Redo and check below

def _random_molecule(additional_properties=False):
    """Returns a 'random' molecule for testing purposes."""
    def rnd_token():
        sel = rnd.randint(1,3)
        if sel == 1: # string token
            chars = string.ascii_letters + string.digits
            res = [rnd.choice(chars) for _ in range(rnd.randint(1,30))]
            # prevent strings with only digits; these would be parsed as integers
            res[rnd.randrange(len(res))] = rnd.choice(string.ascii_letters)
            return ''.join(res)
        elif sel == 2: # integer token
            return rnd.randint(-10000, 10000)
        elif sel == 3: # float token
            return rnd.uniform(-1e10, 1e10)

    k = rnd.randrange(1, 21)

    # element types
    an = [rnd.randrange(1, 119) for _ in range(k)]

    # coordinates
    xyz = [[rnd.uniform(-10,10), rnd.uniform(-10,10), rnd.uniform(-10,10)] for _ in range(k)]

    # molecular properties
    mp = [rnd_token() for _ in range(rnd.randint(0,4))]

    # atomic properties
    ap = [[rnd_token() for _ in range(rnd.randint(0,4))] for _ in range(k)]

    # additional properties
    addp = None
    if additional_properties:
        addp = [[rnd_token() for _ in range(rnd.randint(1,5))] for _ in range(rnd.randint(0,3))]

    return qmml.ExtXYZData(an, xyz, mp, ap, addp)

class TestWriteMolecules:
    # Tests for exporting molecules

    def test_write_simple_single_molecule_to_string(self):
        lhs = qmml.ExtXYZData([1], [[1.,2.,3.]], ["mp", 1, 1.2], [["abc", 12, 1.23]], None)
        rhs = qmml.export_extxyz(lhs)
        rhs = qmml.import_extxyz(rhs)[0]
        assert _equal(lhs, rhs)

    def test_write_random_single_molecule_to_string(self):
        lhs = _random_molecule()
        rhs = qmml.export_extxyz(lhs)
        rhs = qmml.import_extxyz(rhs)[0]
        assert _equal(lhs, rhs)

    def test_write_many_random_molecules_to_file(self):
        lhs = [_random_molecule() for _ in range(10)]
        (fd,fn) = tempfile.mkstemp(suffix = ".xyz"); os.close(fd)
        qmml.export_extxyz(lhs, filename=fn)
        rhs = qmml.import_extxyz(fn)
        os.remove(fn)
        assert all(_equal(a,b) for (a,b) in zip(lhs, rhs))

    def test_write_many_random_molecules_to_list(self):
        lhs = [_random_molecule() for _ in range(10)]
        rhs = [qmml.export_extxyz(m) for m in lhs]
        rhs = qmml.import_extxyz(''.join(rhs))
        assert all(_equal(a,b) for (a,b) in zip(lhs, rhs))

#  #######################
#  #  End of unit tests  #
#  #######################
